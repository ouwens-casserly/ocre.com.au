<?php
/**
 * The Default Template for displaying all Easy Property Listings archive/loop posts with WordPress Themes
 *
 * @package EPL
 * @subpackage Templates/Themes/Default
 * @since 1.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$count 			= $wp_query->found_posts;
$s 				= '';
$status 		= '';
$post_type 		= get_post_type();
$post_object 	= get_post_type_object($post_type);
if ( $count >= 2 ) { $s = 's';}
if( isset($_GET['property_status']) ) { 
	$status = $_GET['property_status'];
}

if($post_type == 'rental') {
	$sale_type = 'Rent';
} elseif ($post_type == 'property') {
	$sale_type = 'Sale';
}

$page_title .= 'Properties for <span>'.$sale_type.'</span>';

if($status == 'Sold' || $status == 'sold') { $page_title = '<span>Sold</span> Properties'; }
if($status == 'Leased' || $status == "leased") { $page_title = '<span>Leased</span> Properties'; }

get_header(); ?>

<div id="content" role="main">

	<div class="row">
	
		<div class="col-md-12">

			<?php get_template_part('global-templates/property-search-form'); ?>
		
		</div>
	
	</div>

	<div class="container">

		<?php if ( have_posts() ) : ?>

		<div class="row">

			<div class="col-md-12">

			<header class="archive-header entry-header loop-header">

				<h1 class="archive-title loop-title text-center display-2 italic"><?php echo $page_title; ?></h1>
					
				<p class="text-center subheading-1"><?php echo $count.' Result'.$s; ?></p>
					
				</header>

			</div>

		</div>

		<div class="entry-content loop-content <?php echo esc_attr( epl_template_class( 'default', 'archive' ) ); ?>">
		
			<?php do_action( 'epl_property_loop_start' ); ?>

			<div class="row post-grid">

				<?php while ( have_posts() ) : the_post(); do_action( 'epl_property_blog' ); endwhile; ?>

				<?php do_action( 'epl_property_loop_end' ); ?>

			</div>

		</div>

		<div class="row">

			<div class="col-md-12">

				<div class="loop-footer">

					<div class="loop-utility clearfix">

						<?php do_action( 'epl_pagination' ); ?>

					</div>

				</div>

			</div>

		</div>

		<?php else : ?>

			<div class="hentry">

				<?php do_action( 'epl_property_search_not_found' ); ?>

			</div>

		<?php endif; ?>

	</div>

</div>

<?php

get_sidebar();

get_footer();
