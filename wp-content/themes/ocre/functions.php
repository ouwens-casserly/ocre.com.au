<?php

/**
 * UnderStrap functions and definitions
 *
 * @package UnderStrap
 */

// Exit if accessed directly.
defined('ABSPATH') || exit;

// UnderStrap's includes directory.
$understrap_inc_dir = get_template_directory() . '/inc';

// Array of files to include.
$understrap_includes = array(
    '/theme-settings.php',                  // Initialize theme default settings.
    '/setup.php',                           // Theme setup and custom theme supports.
    '/widgets.php',                         // Register widget area.
    '/enqueue.php',                         // Enqueue scripts and styles.
    '/template-tags.php',                   // Custom template tags for this theme.
    '/pagination.php',                      // Custom pagination for this theme.
    '/hooks.php',                           // Custom hooks.
    '/extras.php',                          // Custom functions that act independently of the theme templates.
    '/customizer.php',                      // Customizer additions.
    '/custom-comments.php',                 // Custom Comments file.
    '/class-wp-bootstrap-navwalker.php',    // Load custom WordPress nav walker. Trying to get deeper navigation? Check out: https://github.com/understrap/understrap/issues/567.
    '/editor.php',                          // Load Editor functions.
    '/deprecated.php',                      // Load deprecated functions.
    '/epl-settings.php',
    '/agentbox-contacts.php',
    '/vcard.php'
);

// Include files.
foreach ($understrap_includes as $file) {
    require_once $understrap_inc_dir . $file;
}

// Disables the block editor from managing widgets.
add_filter('use_widgets_block_editor', '__return_false');

function understrap_customize_register($wp_customize)
{

    // Add Settings
    $wp_customize->add_setting('transparent_logo', array(
        'transport'         => 'refresh',
        'height'         => 325,
    ));
    $wp_customize->add_setting('customizer_setting_two', array(
        'transport'         => 'refresh',
        'height'         => 325,
    ));

    // Add Section
    $wp_customize->add_section('slideshow', array(
        'title'             => __('Ouwens Casserly Custom items', 'understrap'),
        'priority'          => 70,
    ));

    // Add Controls
    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'transparent_logo_control', array(
        'label'             => __('Transparent Header Logo', 'understrap'),
        'section'           => 'slideshow',
        'settings'          => 'transparent_logo',
    )));
    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'customizer_setting_two_control', array(
        'label'             => __('Banner Image', 'understrap'),
        'section'           => 'slideshow',
        'settings'          => 'customizer_setting_two',
    )));
}
add_action('customize_register', 'understrap_customize_register');


/**
 * Spit out our custom transparent logo so we can have a white AND a black version.
 * This is a duplicate of get_custom_logo (Default wordpress behaviour). 
 * 
 * @since 1.0.0
 */

function get_transparent_logo($blog_id = 0)
{
    $html               = '';
    $switched_blog      = false;

    // $image_url          = get_theme_mod( 'transparent_logo' );
    // $custom_logo_id     = attachment_url_to_postid($image_url);
    $themeurl = get_template_directory_uri() . '/images/ouwens-casserly-white.png';

    // if ( $custom_logo_id ) {
    $custom_logo_attr = array(
        'class' => 'custom-logo',
    );

    // $image_alt = get_post_meta( $custom_logo_id, '_wp_attachment_image_alt', true );

    // if ( empty( $image_alt ) ) {
    //     $custom_logo_attr['alt'] = get_bloginfo( 'name', 'display' );
    // }

    $html = sprintf(
        '<a href="%1$s" id="primary-logo" class="custom-logo-link" rel="home">%2$s</a>',
        esc_url(home_url('/')),
        '<img src="' . $themeurl . '" class="custom-logo" alt="Ouwens Casserly Real Estate" loading="lazy">'
    );

    // } elseif ( is_customize_preview() ) {
    //     $html = sprintf(
    //         '<a href="%1$s" class="custom-logo-link" style="display:none;"><img class="custom-logo"/></a>',
    //         esc_url( home_url( '/' ) )
    //     );
    // }

    return $html;
}

/**
 * Input $args which whill be passed to a WP_Query.
 */
function oc_property_grid($args, $paged = false)
{
    $args['meta_query'][] = [
        'relation' => 'OR',
        [
            'key' => 'hidden_listing',
            'compare' => 'NOT EXISTS'
        ],
        [
            'key' => 'hidden_listing',
            'compare' => '!=',
            'value' => '1'
        ]
    ];

    // we need to check if the agentbox ids are an array and implode
    // $queryValue = $args['meta_query']['value'];
    // if (is_array($queryValue)) {
    //     $value = implode(",", $queryValue);
    // } else $value = $queryValue;
    
    // $cache_key =  'cache_key_' . $args['post_type'] . '_' . $value . '_' . $args['posts_per_page'];

    // if (!$properties = get_transient($cache_key)) {

        $properties = '<div class="container"><div class="row">';

        $query = new WP_Query($args);

        $status = 'For Sale';

        if ($query->have_posts()) {

            while ($query->have_posts()) {

                $query->the_post();
                $prop = get_post_meta(get_the_ID());

                if (get_field('display_street_address')) {
                    $street_address = get_field('display_street_address');
                } else {
                    $street_address = $prop['property_address_street_number'][0] . ' ' . $prop['property_address_street'][0];
                }

                $sold_date = '';
                if ($prop['property_status'][0] == 'sold') {
                    $sold_date = 'Sold on ' . date('jS \of F Y', strtotime($prop['property_sold_date'][0]));
                    if ($prop['property_sold_price_display'][0] == 'yes') {
                        $sold_date .= ' for $' . number_format($prop['property_sold_price'][0]);
                    }
                } else {
                    $sold_date = wp_kses_post(epl_get_property_price());
                }

                if (has_post_thumbnail()) {
                    $img_url = get_the_post_thumbnail_url(get_the_ID(), 'property-grid');
                } else {
                    $img_url = get_template_directory_uri() . '/media/placeholder.jpg';
                }

                if ($prop['property_status'][0] == 'offmarket' || $prop['property_com_exclusivity'][0] == '1') {
                    $exclusive = '<span class="oc-exclusive-label">OC Exclusive</span>';
                } else {
                    $exclusive = '';
                }

                $properties .=
                    '<div class="col-xs-1-12 col-md-4 mb-20 property-grid-item">
                    <div class="polaroid-property">
                        <a href="' . get_permalink() . '">
                            ' . wp_kses_post(epl_get_price_sticker()) . '
                            <img src="' . $img_url . '" class="property-image" />
                            ' . $exclusive . '
                            <div class="property-content">
                                <div class="property-header">
                                    <h5 class="">' . $street_address . '</h5>
                                    <p class="suburb">' . $prop['property_address_suburb'][0] . '</p>
                                    <p class="caption">' . $prop['property_heading'][0] . '</p>
                                </div>
                                <div class="property-footer">
                                    <p class="property-price">' . $sold_date . '</p>
                                    <div class="features">' . get_property_features() . '</div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>';
            }
        }
        $properties .= '</div></div>';
        // set_transient($cache_key, $properties, 24 * HOUR_IN_SECONDS);
    // }

    echo $properties;

    wp_reset_postdata();
}

/**
 * Input $args which whill be passed to a WP_Query.
 */
function get_single_property($ID = '', $strip = false)
{

    $args = [
        'post_type' => 'any',
        'p' => $ID
    ];

    $query = new WP_Query($args);

    if ($query->have_posts()) {

        $properties = '';

        while ($query->have_posts()) {
            $query->the_post();

            $prop = get_post_meta(get_the_ID());

            if (get_post_type(get_the_ID()) == 'property') {
                $price = $prop['property_price_view'][0];
            } elseif (get_post_type(get_the_ID()) == 'rental') {
                $price = '$' . $prop['property_rent'][0] . ' / ' . $prop['property_rent_period'][0];
            }

            if ($prop['property_status'][0] == 'sold' || $prop['property_status'][0] == 'leased') {
                $status = "<span class='status-" . $prop["property_status"][0] . "'>" . $prop["property_status"][0] . "</span>";
            } else {
                $status = "";
            }

            $type = get_post_type();
            if (has_post_thumbnail()) {
                $img_url = get_the_post_thumbnail_url(get_the_ID(), 'property-grid');
            } else {
                $img_url = '';
            }

            $features = [
                'Bed' => $prop['property_bedrooms'][0],
                'Bath' => $prop['property_bathrooms'][0],
                'Car' => $prop['property_garage'][0]
            ];

            $features_list = '';
            foreach ($features as $key => $feature) {
                if ($feature != '' && $feature != '0') {
                    $features_list .= "<div class='feature'><span class='display-1 small'>" . $feature . "</span><span class='" . $key . "' alt='" . $key . "' />" . $key . "</span></div>";
                }
            }

            if ($prop['property_status'][0] == 'offmarket') {
                $exclusive = "<span class='oc-exclusive-label'>OC Exclusive</span>";
            } else {
                $exclusive = '';
            }

            $properties .= "
                <div class='gmap-property property-grid-item'>
                    <div class='polaroid-property'>
                        <a href='" . get_permalink() . "'>"
                . $status .
                "<img src='" . $img_url . "' class='property-image' />"
                . $exclusive .
                "<div class='property-content'>
                                <div class='property-header'>
                                    <h5 class=''>" . $prop["property_address_street_number"][0] . " " . $prop["property_address_street"][0] . "</h5>
                                    <p class='suburb'>" . $prop["property_address_suburb"][0] . "</p>
                                    <p class='caption'>" . $prop["property_heading"][0] . "</p>
                                </div>
                                <div class='property-footer'>
                                    <p class='property-price'>" . $price . "</p>
                                    <div class='features'>" . $features_list . "</div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>";

            if ($strip = true) {
                $properties = preg_replace("/\r|\n/", "", $properties);
            }
        }
    }
    wp_reset_postdata();

    return $properties;
}

function my_acf_google_map_api($api)
{
    $api['key'] = 'AIzaSyBYbX0BCfzLojcxRCAi0BCne_yn9B6y2hY';
    return $api;
}
add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');

/**
 * Input $args which whill be passed to a WP_Query.
 */
function oc_office_grid($args, $item_class = 'col-xs-1-12 col-md-4 mb-5', $size = 'large')
{

    $contact_fields = [
        'street_number',
        'street_name',
        'city',
        'state',
        'post_code'
    ];

    $query = new WP_Query($args);

    if ($query->have_posts()) {

        while ($query->have_posts()) {
            $query->the_post();

            $location = get_field('address');

            if ($location) {

                foreach ($contact_fields as $i => $k) {

                    if (isset($location[$k])) {

                        if ($k == 'street_name' || $k == 'city') {
                            $address .= sprintf('<span class="segment-%s">%s</span>, ', $k, $location[$k]);
                        } else {
                            $address .= sprintf('<span class="segment-%s">%s</span> ', $k, $location[$k]);
                        }
                    }
                }
                $address = trim($address, ', ');
            }

            if (get_field('opening_hours')) {
                $opening_hours = 'Open ';
                $opening_hours .= get_field('opening_hours');
            } else {
                $opening_hours = '';
            }

            // start contact details custom fields
            $contact_details = get_field('contact_details', false);

            $contact_info = '';

            $link_types = ['phone', 'sales', 'rentals'];

            if ($contact_details) {

                foreach ($contact_details as $key => $value) {

                    if (!empty($value)) {

                        if (in_array($key, $link_types)) {

                            $contact_info .= '<p class="' . $key . '">' . $key . ': <a href="tel: ' . $value . '"> ' . $value . '</a></p>';
                        } else {

                            if ($key == 'email') {
                                $contact_info .= '<p class="' . $key . '">' . $key . ': <a href="mailto: ' . $value . '"> ' . $value . '</a></p>';
                            } else {
                                $contact_info .= '<p class="' . $key . '"> ' . $key . ': ' . $value . '</p>';
                            }
                        }
                    }
                }
            }

            $prop = get_post_meta(get_the_ID());
            $type = get_post_type();
            if (has_post_thumbnail()) {
                $img_url = get_the_post_thumbnail_url(get_the_ID(), 'large');
            }

            $properties .=
                '<div class="' . $item_class . ' property-grid-item">
                <div class="polaroid-property">
                    <a href="' . get_permalink() . '" style="height: auto;">
                        <img src="' . $img_url . '" class="property-image" />
                    </a>
                    <div class="property-content">
                        <div class="upper">
                            <div class="property-header">
                                <h5 class="">' . get_the_title() . '</h5>
                                <p class="office-address">' . $address . '</p>
                            </div>
                            <div class="contact-info">' . $contact_info . '</div>
                            <div class="opening-hours">' . $opening_hours . '</div>
                        </div>
                        <div class="contact-buttons">
                            <a href="' . get_permalink() . '" class="btn btn-outline-primary btn-sm">Contact Us</a>
                            <a href="' . get_permalink() . '/#single-office-team" class="btn btn-link btn-sm">Meet the team</a>
                        </div>
                    </div>
                </div>
            </div>';
            $address = '';
        }
    }
    wp_reset_postdata();

    return $properties;
}


/** 
 * 
 * Gets the address from the ACF field
 * to be used elsewhere.
 * 
 * */
function oc_get_address()
{
    // Start location custom field
    $location = get_field('address');

    if ($location) {

        $address = '';
        foreach (array('street_number', 'street_name', 'city', 'state', 'post_code') as $i => $k) {
            if (isset($location[$k])) {
                $address .= sprintf('<span class="segment-%s">%s</span>, ', $k, $location[$k]);
            }
        }
        $address = trim($address, ', ');
    } // End location custom field

    return $address;
}

/**
 * Get contact details for office locations
 * 
 */

function oc_get_contact_details()
{

    if (get_field('opening_hours')) {
        $opening_hours = 'Open ';
        $opening_hours .= get_field('opening_hours');
    } else {
        $opening_hours = '';
    }

    // start contact details custom fields
    $contact_details = get_field('contact_details');

    $contact_info = '<div class="contact-info">';

    $link_types = ['phone', 'sales', 'rentals'];

    if ($contact_details) {

        foreach ($contact_details as $key => $value) {

            if (!empty($value)) {

                if (in_array($key, $link_types)) {
                    $contact_info .= '<div class="' . $key . '"><span class="caption">' . $key . '</span> <a href="tel: ' . $value . '"> ' . $value . '</a></div>';
                } else {

                    if ($key == 'email') {
                        $contact_info .= '<div class="' . $key . '"><span class="caption">' . $key . '</span> <a href="mailto: ' . $value . '"> ' . $value . '</a></div>';
                    } else {
                        $contact_info .= '<div class="' . $key . '"><span class="caption"> ' . $key . '</span> ' . $value . '</div>';
                    }
                }
            }
        }
    }

    $contact_info .= '</div>';

    return $contact_info;
}

function update_contact_methods($contactmethods)
{

    unset($contactmethods['office_phone']);
    unset($contactmethods['linkedin']);
    unset($contactmethods['skype']);
    unset($contactmethods['pinterest']);
    unset($contactmethods['myspace']);
    unset($contactmethods['soundcloud']);
    unset($contactmethods['tumblr']);
    unset($contactmethods['wikipedia']);

    return $contactmethods;
}
add_filter('user_contactmethods', 'update_contact_methods');

if (is_admin()) {
    remove_action("admin_color_scheme_picker", "admin_color_scheme_picker");
}

function hide_personal_options()
{
    echo "\n" . '<script type="text/javascript">jQuery(document).ready(function($) { $(\'form#your-profile > h3:first\').hide(); $(\'form#your-profile > table:first\').hide(); $(\'form#your-profile\').show(); });</script>' . "\n";
    echo "<script>jQuery(document).ready(function()    
    {jQuery('#url').parents('tr').remove();});</script>";
}
add_action('admin_head', 'hide_personal_options');

add_filter('wp_is_application_passwords_available', '__return_false');

add_action('admin_head', 'lubus_hide_yoast_profile');

function lubus_hide_yoast_profile()
{
    echo '<style>
          .yoast-settings {
               display: none;
          }
        </style>';
}

function oc_team_list($positions = '', $layout = 'col-md-3 col-lg-3 single-team-member col-6', $location = [])
{

    $meta_query = [
        'relation' => 'AND',
        [
            'key' => 'team_type',
            'value' => $positions,
            'compare' => 'LIKE'
        ]
    ];

    if ($location != 'none') {
        $meta_query[] = [
            'key' => 'office',
            'value' => $location,
            'compare' => 'LIKE'
        ];
    }

    $args = [
        'order' => 'ASC',
        'orderby' => 'display_name',
        'meta_query' => $meta_query,
        'role' => 'Author',
    ];

    $user_query = new WP_User_Query($args);

    if (!empty($user_query->get_results())) :

        if ($title) : echo '<h2 class="mb-5">' . $title . '</h2>';
        endif;

        echo '<div id="team-list" class="row">';

        $user_group = [];

        foreach ($user_query->get_results() as $user) :

            echo agent_business_card($user->ID, $layout);

        endforeach;

        echo '</div>';

    else :

        echo 'No users found.';

    endif;
}

function agent_business_card($user_ID = '', $layout = '')
{

    // Get the Global EPL object for the Author.
    global $epl_author;

    // Exit if we aren't an author.
    if (!$epl_author) {
        return;
    }

    // Gather all the user data we need
    $user_data = get_userdata($user_ID);
    $user_link = get_author_posts_url($user_ID);
    $static_profile_image = get_field('static_profile_image', 'user_' . $user_ID);
    if (empty($static_profile_image)) {
        $static_profile_image['sizes']['medium'] = get_template_directory_uri() . '/images/profile.jpg';
    }

    // Container for card + Image link.
    $agent_card = '
    <div id="user-' . $user_ID . '" class="' . $layout . ' single-team-member">
        <div class="polaroid-property">
            <a href="' . $user_link . '" alt="' . $user_data->display_name . '">
                <img src="' . $static_profile_image['sizes']['medium'] . '" alt="' . $static_profile_image['alt'] . '" />
            </a>
            <div class="entry-header entry-content author-contact-details">
                <h4 class="author-title">
                    <a href="' . $user_link . '" title="' . $user_data->display_name . '">' . $user_data->display_name . '</a>
                </h4>
                <div class="author-position">
                    <p class="position">' . $user_data->position . '</p>
                </div>
            </div>
        </div>
    </div>'; //Closing DIV's for #user- and epl-author-contact-details.

    return $agent_card;
}

function oc_agent_meta($user_ID = '', $layout = '')
{

    // Get the Global EPL object for the Author.
    global $epl_author;

    // Exit if we aren't an author.
    if (!$epl_author) {
        return;
    }

    // Gather all the user data we need
    $user_data = get_userdata($user_ID);
    $user_meta = get_user_meta($user_ID);
    $user_link = get_author_posts_url($user_ID);
    $static_profile_image = get_field('static_profile_image', 'user_' . $user_ID);

    /**
     * Some mobile numbers on users are entered with 'tel:' so we check 
     * and add it to those numbers without it so the 'call' button always
     * works.
     */

    if ($user_meta['mobile'][0] == '') {
        $phone_button = '';
    } else {
        if (strpos(strtolower($user_meta['mobile'][0]), 'tel') !== false) {
            $mobile_number = $user_meta['mobile'][0];
        } else {
            $mobile_number = 'tel:' . $user_meta['mobile'][0];
        }
        $desktop_phone_display = str_replace('tel', '', $mobile_number);
        $desktop_phone_display = str_replace(':', '', $desktop_phone_display);
        $phone_button = '
        <div class="btn-group mr-2" role="group" aria-label="First group">
            <a href="' . $mobile_number . '" type="button" class="btn btn-primary btn-sm d-md-none">Call</a>
            <a href="' . $mobile_number . '" type="button" class="btn btn-primary btn-sm d-none d-md-block">' . $desktop_phone_display . '</a>

        </div>';
    }

    // Container for card + Image link.
    $agent_card = '
    <div id="user-' . $user_ID . '" class="' . $layout . ' single-team-member-meta">
        <div class="polaroid-property">
            <div class="entry-header entry-content author-contact-details">
                <h4 class="author-title">
                    <a href="' . $user_link . '" title="' . $user_data->display_name . '">' . $user_data->display_name . '</a>
                </h4>
                <div class="author-position">
                    <p class="position">' . $user_data->position . '</p>
                </div>
                <div class="" role="toolbar"">
                ' . $phone_button . '
                <div class="btn-group mr-2" role="group">
                    <a href="mailto:' . $user_data->user_email . '" type="button" class="btn btn-outline-primary btn-sm">Email me</a>
                </div>
            </div>
            </div>
            <a href="' . $user_link . '" alt="' . $user_data->display_name . '">
                <img src="' . $static_profile_image['sizes']['medium'] . '" alt="' . $static_profile_image['alt'] . '" class="agent-image"/>
            </a>
        </div>

    </div>'; //Closing DIV's for #user- and epl-author-contact-details.
    wp_reset_postdata();
    return $agent_card;
}

add_filter('gform_pre_render_1', 'add_readonly_script');
function add_readonly_script($form)
{
?>
    <script type="text/javascript">
        jQuery(document).ready(function() {
            /* apply only to a input with a class of gf_readonly */
            jQuery(".gf_readonly input").attr("readonly", "readonly");
        });
    </script>
<?php
    return $form;
}

function searchfilter($query)
{

    if ($query->is_search && !is_admin()) {
        $query->set('post_type', array('post'));
    }

    return $query;
}

add_filter('pre_get_posts', 'searchfilter');

function wpdocs_custom_excerpt_length($length)
{
    return 30;
}
add_filter('excerpt_length', 'wpdocs_custom_excerpt_length', 999);


function custom_pre_get_posts($query)
{
    if ($query->is_main_query() && !$query->is_feed() && !is_admin() && is_category()) {
        $query->set('paged', str_replace('/', '', get_query_var('page')));
    }
}

add_action('pre_get_posts', 'custom_pre_get_posts');

function custom_request($query_string)
{
    if (isset($query_string['page'])) {
        if ('' != $query_string['page']) {
            if (isset($query_string['name'])) {
                unset($query_string['name']);
            }
        }
    }
    return $query_string;
}

add_filter('request', 'custom_request');

add_filter('get_the_archive_title', function ($title) {
    if (is_category()) {
        $title = single_cat_title('Real Life Blog: <span>', false) . '</span>';
    } elseif (is_tag()) {
        $title = single_tag_title('Real Life Blog: <span>', false) . '</span>';
    } elseif (is_author()) {
        $title = '<span class="vcard">' . get_the_author() . '</span>';
    } elseif (is_tax()) { //for custom post types
        $title = sprintf(__('%1$s'), single_term_title('', false));
    } elseif (is_post_type_archive()) {
        $title = post_type_archive_title('', false);
    }
    return $title;
});


// stop wordpress removing <span> tags
function uncoverwp_tiny_mce_fix($init)
{
    // html elements being stripped
    $init['extended_valid_elements'] = 'span[*]';

    // pass back to wordpress
    return $init;
}
add_filter('tiny_mce_before_init', 'uncoverwp_tiny_mce_fix');




/**
 * Custom Author Base
 *
 * @return void
 */
function change_author_base()
{
    global $wp_rewrite;
    $wp_rewrite->author_base = 'meet-the-team';
    $wp_rewrite->author_structure = 'meet-the-team/%author%/';
}
add_action('init', 'change_author_base', 99);

/* Remove Yoast SEO Add custom title or meta template variables
 * Credit: Matt Neal
 *******
 * NOTE: The snippet preview in the backend will show the custom variable '%%myname%%'.
 * However, the source code of your site will show the output of the variable 'My name is Moses'.
 */

// define the custom replacement callback
function get_suburb()
{

    global $author;
    $agent = get_fields('user_' . $author);
    $user_meta     = get_user_meta($author); // Meta fields only.


    if ($user_meta['position'][0] == 'Sales Agent') $user_meta['position'][0] = 'Real Estate Agent';

    if (get_field('core_market_suburb', 'user_' . $author)) :

        return $user_meta['position'][0] . ' in ' . $agent['core_market_suburb'][0]->name;

    else :

        return $user_meta['position'][0] . ' in Adelaide';

    endif;
}

function register_custom_yoast_variables()
{

    wpseo_register_var_replacement('%%coremarketsuburb%%', 'get_suburb', 'advanced', 'Outputs the field suburb');
}
add_action('wpseo_register_extra_replacements', 'register_custom_yoast_variables');

add_filter('wpseo_opengraph_image_size', 'yoast_seo_opengraph_change_image_size');
function yoast_seo_opengraph_change_image_size()
{

    return 'large';
}

// the ajax function
add_action('wp_ajax_data_fetch', 'data_fetch');
add_action('wp_ajax_nopriv_data_fetch', 'data_fetch');
function data_fetch()
{

    $search_string = esc_attr($_POST['keyword']);
    if (strlen($search_string) < 3) {
        die();
    }
    $users = new WP_User_Query(array(
        'search'         => "*{$search_string}*",
        'number'         => 3,
        'search_columns' => array(
            'user_login',
            'user_nicename',
            'user_email',
            'user_url',
        ),
        'meta_query' => array(
            'relation' => 'OR',
            array(
                'key'     => 'first_name',
                'value'   => $search_string,
                'compare' => 'LIKE'
            ),
            array(
                'key'     => 'last_name',
                'value'   => $search_string,
                'compare' => 'LIKE'
            )
        )
    ));

    if (!empty($users->get_results())) :

        foreach ($users->get_results() as $user) :

            $user_meta = get_user_meta($user->ID);
            $static_profile_image = get_field('static_profile_image', 'user_' . $user->ID);
            $author_url = get_author_posts_url($user->ID);

            if ($search_string == '') {
                die();
            }

            echo '
            <a href="' . $author_url . '">
                <div class="single-author-search-result">
                    <img src="' . $static_profile_image['sizes']['thumbnail'] . '">
                    <div class="author-info">
                        <div class="display-name">' . $user->display_name . '</div>
                        <div class="search-position">' . $user_meta['position'][0] . '</div>
                    </div>
                </div>
            </a>';

        //var_dump($user);
        endforeach;

    endif;

    die();
}

function get_property_locations($property_type = 'property')
{

    $coordinates = '[';
    $args = [
        'post_type'         => $property_type,
        'order'             => 'DESC',
        'orderby'           => 'date',
        'posts_per_page'    => -1,
        'meta_query' => [
            [
                'key'     => 'property_status',
                'value'   => 'current',
                'compare' => 'LIKE',
            ]
        ]
    ];
    $property_list = new WP_Query($args);

    if ($property_list->have_posts()) :

        while ($property_list->have_posts()) :

            $property_list->the_post();

            $property_meta = get_post_meta(get_the_ID());

            $title = get_the_title();
            $img_url = get_the_post_thumbnail_url(get_the_ID(), 'property-grid');

            if (!empty($property_meta['property_address_coordinates'][0])) {
                $coordinate_array = explode(",", $property_meta['property_address_coordinates'][0], 2);

                $coordinates .= '[
                    "' . $link . '", 
                    { lat: ' . $coordinate_array[0] . ', lng: ' . $coordinate_array[1] . ' },
                    "' . $title . '",
                    "' . $img_url . '",
                    "' . get_single_property(get_the_ID(), true) . '",
                
                ],';
            }


        endwhile;

        wp_reset_postdata();

    endif;

    wp_reset_postdata();

    $coordinates .= ']';

    return $coordinates;
}

function get_property_locations_json($property_type = 'rental')
{

    $args = [
        'post_type'         => $property_type,
        'posts_per_page'    => -1,
        'no_found_rows'         => false,
        'meta_query' => [
            'relation' => 'AND',
            [
                'key'     => 'property_status',
                'value'   => 'current',
                'compare' => '=',
            ],
            [
                'key' => 'hidden_listing',
                'compare' => '!=',
                'value' => '1'
            ]
        ]
    ];

    $cache_key =  'gmaps_cache_' . $args['post_type'] . '_' . $args['meta_query'][0]['value'];

    if (!$json_coordinates = get_transient($cache_key)) {

        $property_list = new WP_Query($args);

        if ($property_list->have_posts()) :

            while ($property_list->have_posts()) : $property_list->the_post();

                $property_meta  = get_post_meta(get_the_ID());
                $title          = get_the_title();
                $img_url        = get_the_post_thumbnail_url(get_the_ID(), 'property-grid');

                if (!empty($property_meta['property_address_coordinates'][0])) {

                    $coordinate_array = explode(",", $property_meta['property_address_coordinates'][0], 2);
                    $property_html = rawurlencode(get_single_property(get_the_ID(), true));

                    $json_coordinates[] = [
                        'lat' => $coordinate_array[0],
                        'lng' => $coordinate_array[1],
                        'property_id' => $property_html
                    ];
                }

            endwhile;

        endif;

        $json_coordinates = json_encode($json_coordinates);

        set_transient($cache_key, $json_coordinates, 24 * HOUR_IN_SECONDS);
    }

    wp_reset_postdata();
    return $json_coordinates;
}


function get_property_measurement($land_area_unit_sub)
{
    switch ($land_area_unit_sub) {
        case 'square':
            $land_area_unit = 'm<sup>2</sup>';
            break;
        case 'squareMeter':
            $land_area_unit = 'm<sup>2</sup>';
            break;
        case 'acre':
            $land_area_unit = '<sup>acres</sup>';
            break;
        case 'hectare':
            $land_area_unit = '<sup>ha</sup>';
            break;
        case 'sqft':
            $land_area_unit = '<sup>sq. ft.</sup>';
            break;
        default:
            $land_area_unit = 'm<sup>2</sup>';
    }
    return $land_area_unit;
}

/** 
 *  Generate Property Features
 */

function get_property_features($property_features = array('Bed', 'Bath', 'Car'))
{

    global $property;
    $features = [];

    if (in_array('Bed', $property_features)) {
        $features['Bed'] = $property->get_property_meta('property_bedrooms');
    }

    if (in_array('Bath', $property_features)) {
        $features['Bath'] = $property->get_property_meta('property_bathrooms');
    }

    if (in_array('Car', $property_features)) {
        $features['Car'] = intval($property->get_property_meta('property_garage'));
        $features['Car'] = $features['Car'] + intval($property->get_property_meta('property_carport'));
        $features['Car'] = $features['Car'] + intval($property->get_property_meta('property_open_spaces'));
    }

    if (in_array('Land', $property_features) && $property->post_type == 'property') {

        if ($property->get_property_meta('property_land_area') == 0) {

            if ($property->get_property_meta('property_building_area') == 0) {
                // Do nothing
            } else {
                $property->meta['property_land_area'][0] = $property->get_property_meta('property_building_area');
                $land_area_unit = get_property_measurement($property->get_property_meta('property_building_area_unit'));
                $features['Building'] = $property->get_property_meta('property_land_area') . $land_area_unit;
            }
        } else {
            $land_area_unit = get_property_measurement($property->get_property_meta('property_land_area_unit'));
            $features['Land'] = get_property_meta('property_land_area') . $land_area_unit;
        }
    }

    $features_list = '';
    foreach ($features as $key => $feature) {
        if ($feature != '' && $feature != '0') {
            $features_list .= '
            <div class="feature">
                <span class="display-1 small">' . $feature . '</span>
                <span class="' . $key . '" alt="' . $key . '" />' . $key . '</span>
            </div>';
        }
    }
    wp_reset_postdata();
    return $features_list;
}

/**
 * Get property authority.
 */
function get_property_authority()
{
    global $property;

    if ($property->meta['property_authority'][0]) {
        return $property->meta['property_authority'][0];
    } else {
        return 'Rental';
    }
}


/**
 * If the post type is 'property' (for sale properties) and
 * the property isn't sold, generate a 'make an offer' button.
 * This requires a page called /make-an-offer/ with a gravity
 * form that grabs the data and prefills the form. The form is
 * a gravity form version of the "make an offer" document most
 * agencies have in PDF form.
 */
function make_an_offer_button()
{

    global $property;
    $make_an_offer = '';
    $agent = $property->get_property_meta('property_agent');
    $address = get_the_title();
    $property_id = get_the_ID();
    $price = $property->get_property_meta('property_price_view');
    $agent_email = get_the_author_meta('user_email');

    if ($property->post->post_type == 'property' && $property->meta['property_status'][0] != 'sold') {

        $make_an_offer .= '
        <div class="offer-button">
            <a href="/make-an-offer/?' .
            'agent=' . $agent .
            '&property_address=' . $address .
            '&property_id=' . $property_id .
            '&price=' . $price .
            '&agent_email=' . $agent_email .
            '" class="btn btn-outline-success btn-sm mb-2">Make an offer' .
            '</a>' .
            '</div>';
    }
    return $make_an_offer;
}

/**
 * Exclude all posts from any queries that are marked as hidden.
 */

function hide_hidden_listings($query)
{
    if (is_admin()) {
        return;
    }

    if (is_archive() && in_array($query->get('post_type'), array('property', 'rental'))) {
        $meta_query = $query->get('meta_query', array());

        $meta_query[] = [
            'relation' => 'OR',
            [
                'key' => 'hidden_listing',
                'compare' => 'NOT EXISTS'
            ],
            [
                'key' => 'hidden_listing',
                'compare' => '!=',
                'value' => '1'
            ]
        ];
        $query->set('meta_query', $meta_query);
    }

    return $query;
}
add_action('pre_get_posts', 'hide_hidden_listings');


add_filter('nav_menu_link_attributes', 'oc_popup_oc_first', 10, 3);
function oc_popup_oc_first($atts, $item, $args)
{
    // The ID of the target menu item
    $menu_target = 5491;

    // inspect $item
    if ($item->ID == $menu_target) {
        $atts['data-toggle'] = 'modal';
    }
    return $atts;
}

function my_property_available_dates()
{
    global $property;
    if ('rental' == $property->post_type && $property->get_property_meta('property_date_available') != '' && $property->get_property_meta('property_status') != 'leased') {
        $date = $property->get_property_meta('property_date_available');
        $date = strtotime($date);

        echo '<div class="property-meta date-available">Available: ';

        if (current_time('timestamp') <  $date) {
            echo date('l, jS F', $date);
        } else {
            echo 'Now';
        }
        echo '</div>';
    }
}
add_action('epl_property_available_dates', 'my_property_available_dates');
remove_action('epl_property_available_dates', 'epl_property_available_dates');

// <div class="property-meta date-available">Available from Monday, 27th September at 12:00 am</div>

/* output the agent custom photo instead of the gravatar */
function custom_category_og_image()
{

    if (is_author()) :
        global $author;
        $user_meta     = get_user_meta($author); // Meta fields only.

        $auth_id = get_the_author_meta('ID');
        $agent = get_fields('user_' . $auth_id);
        if (get_field('static_profile_image', 'user_' . $author)) :
            return $agent['static_profile_image']['url'];
        else :
            return $agent['banner_image']['url'];
        endif;



    endif;
}
add_filter('wpseo_opengraph_image', 'custom_category_og_image', 10, 0);

function my_epl_disable_feature_links()
{
    return false;
}
add_filter('epl_features_taxonomy_link_filter', 'my_epl_disable_feature_links');



function add_auction_price($price)
{
    global $property;

    $price_display = $property->get_property_meta('property_price_display');
    $display_price = $property->get_property_meta('property_price_view');
    if ($property->get_property_meta('property_price_display') === 'yes' && !empty($property->get_property_meta('property_price_view')) && $property->get_property_meta('property_authority') === 'auction' && $property->get_property_meta('property_status') != 'sold') {
        echo '<span class="page-price auction strong" style="font-weight: 500; font-size: 18px;">' . $display_price . '</span>';
    }
}
add_filter('epl_property_price_content', 'add_auction_price', 100);

function redirect_if_property_id_exists()
{

    if (is_404()) {

        // Get the Request URI, but remove the query string, leaving us with the ID number.
        $unique_ID = strtok(trim($_SERVER['REQUEST_URI'], '/'), '?');

        $property_args = [
            'post_type'         => ['property', 'rental'],
            'meta_query' => [
                'relation' => 'OR',
                [
                    'key'     => 'property_unique_id',
                    'value'   => $unique_ID,
                    'compare' => 'LIKE',
                ],
                [
                    'key'     => 'agentbox_unique_id',
                    'value'   => $unique_ID,
                    'compare' => 'LIKE',
                ]
            ]
        ];

        $unique_id_property = new WP_Query($property_args);
        if ($unique_id_property->have_posts()) {
            while ($unique_id_property->have_posts()) {
                $unique_id_property->the_post();
                $new_url = get_permalink();
                wp_redirect($new_url . '?unlisted=true');
            }
        } else {
            if (isset($_GET['token'])) {
                $all_content = get_agentbox_content($unique_ID, true);
                var_dump($all_content);
                die();
                $property_unique_args = [
                    'post_type' => ['property', 'rental'],
                    'meta_query' => [
                        [
                            'key'     => 'property_unique_id',
                            'value'   => $all_content->response->listing->subOfficeListingId,
                            'compare' => '=',
                        ]
                    ]
                ];
                $final_try = new WP_Query($property_unique_args);
                if ($final_try->have_posts()) {
                    while ($final_try->have_posts()) {
                        $final_try->the_post();
                        $new_url = get_permalink();
                        wp_redirect($new_url . '?unlisted=true');
                    }
                }
            }
        }
    }
}
add_action('template_redirect', 'redirect_if_property_id_exists');

/**
 * Enable the ability to search via epl_search for pet friendly.
 */
function my_epl_custom_search_widget_fields_frontend_callback($array)
{
    $array[] = array(
        'key'            =>    'search_other',
        'meta_key'        =>    'property_pet_friendly',
        'label'            =>    __('Pet Friendly', 'epl'),
        'type'            =>    'checkbox',
        'exclude'        =>    array('land', 'commercial', 'commercial_land', 'business'),
        'query'    =>    array(
            'query' => 'meta',
            'compare' => 'IN',
            'value' => array(
                'yes',
                '1'
            )
        ),

    );
    return $array;
}
add_filter('epl_search_widget_fields_frontend', 'my_epl_custom_search_widget_fields_frontend_callback');

/**
 * Extend get terms with post type parameter.
 *
 * @global $wpdb
 * @param string $clauses
 * @param string $taxonomy
 * @param array $args
 * @return string
 */
function df_terms_clauses($clauses, $taxonomy, $args)
{
    if (isset($args['post_type']) && !empty($args['post_type']) && $args['fields'] !== 'count') {
        global $wpdb;

        $post_types = array();

        if (is_array($args['post_type'])) {
            foreach ($args['post_type'] as $cpt) {
                $post_types[] = "'" . $cpt . "'";
            }
        } else {
            $post_types[] = "'" . $args['post_type'] . "'";
        }

        if (!empty($post_types)) {
            $clauses['fields'] = 'DISTINCT ' . str_replace('tt.*', 'tt.term_taxonomy_id, tt.taxonomy, tt.description, tt.parent', $clauses['fields']) . ', COUNT(p.post_type) AS count';
            $clauses['join'] .= ' LEFT JOIN ' . $wpdb->term_relationships . ' AS r ON r.term_taxonomy_id = tt.term_taxonomy_id LEFT JOIN ' . $wpdb->posts . ' AS p ON p.ID = r.object_id';
            $clauses['where'] .= ' AND (p.post_type IN (' . implode(',', $post_types) . ') OR p.post_type IS NULL)';
            $clauses['orderby'] = 'GROUP BY t.term_id ' . $clauses['orderby'];
        }
    }
    return $clauses;
}

add_filter('terms_clauses', 'df_terms_clauses', 10, 3);

add_action('before_delete_post', 'delete_all_attached_media');

function delete_all_attached_media($post_id)
{

    if (get_post_type($post_id) == "property" || get_post_type($post_id) == "rental") {
        $attachments = get_attached_media('', $post_id);

        foreach ($attachments as $attachment) {
            wp_delete_attachment($attachment->ID, 'true');
        }
    }
}

// Stop users from accessing the admin dashboard unless you are an admin.
function prevent_author_access()
{
    if (current_user_can('author') && is_admin()) {
        // do something here. maybe redirect to homepage
        wp_safe_redirect(get_bloginfo('url'));
    }
}
add_action('admin_init', 'prevent_author_access');

// Remove the admin bar for all users except for admins.
add_action('after_setup_theme', 'remove_admin_bar');
function remove_admin_bar()
{
    if (!current_user_can('administrator') && !is_admin()) {
        show_admin_bar(false);
    }
}

function get_ocre_property_suburb($full = true, $street_separator = true, $separator_symbol = ',')
{

    global $property, $epl_settings;

    if (!is_bool($full)) {
        $full = true;
    }

    $epl_property_address_separator        = apply_filters('epl_property_address_separator', ',');
    $epl_property_address_separator_suburb = apply_filters('epl_property_address_separator_suburb', false);
    $epl_property_address_separator_city   = apply_filters('epl_property_address_separator_city', false);

?>

    <?php
    if (true === $full) {
    ?>
        <span class="entry-title-sub">
            <?php
            if ('commercial' === $property->post_type || 'business' === $property->post_type) {
                if ('yes' === $property->get_property_meta('property_com_display_suburb') || 'yes' === $property->get_property_meta('property_address_display')) {
            ?>
                    <span class="item-suburb"><?php echo esc_attr($property->get_property_meta('property_address_suburb')); ?></span>
                <?php
                    if (true === $epl_property_address_separator_suburb && strlen(trim($property->get_property_meta('property_address_suburb')))) {
                        echo '<span class="item-separator">' . esc_attr($epl_property_address_separator) . '</span>';
                    }
                }
            } else {
                ?>
                <span class="item-suburb"><?php echo esc_attr($property->get_property_meta('property_address_suburb')); ?></span>
            <?php
                if (true === $epl_property_address_separator_suburb && strlen(trim($property->get_property_meta('property_address_suburb')))) {
                    echo '<span class="item-separator">' . esc_attr($epl_property_address_separator) . '</span>';
                }
            }
            ?>

            <?php
            if ('yes' === $property->get_epl_settings('epl_enable_city_field')) {
            ?>
                <span class="item-city"><?php echo esc_attr($property->get_property_meta('property_address_city')); ?></span>
            <?php
                if (true === $epl_property_address_separator_city && strlen(trim($property->get_property_meta('property_address_city')))) {
                    echo '<span class="item-separator">' . esc_attr($epl_property_address_separator) . '</span>';
                }
            }
            ?>
            <span class="item-state"><?php echo esc_attr($property->get_property_meta('property_address_state')); ?></span>
            <span class="item-pcode"><?php echo esc_attr($property->get_property_meta('property_address_postal_code')); ?></span>
            <?php
            if ('yes' === $property->get_epl_settings('epl_enable_country_field')) {
            ?>
                <span class="item-country"><?php echo esc_attr($property->get_property_meta('property_address_country')); ?></span>
            <?php
            }
            ?>
        </span>
<?php
    }
}
add_action('ocre_property_suburb', 'get_ocre_property_suburb', 10, 3);

function get_agentbox_content($property_id, $return_body = false)
{

    if (empty($property_id)) {
        return 'no id found';
    }

    $response = wp_remote_get(
        'https://api.agentboxcrm.com.au/listings/' . $property_id . '?include=documents&version=2',
        [
            'headers' => [
                'Content-Type' => 'application/json',
                'X-Client-ID' => 'aHR0cHM6Ly9vY3JlbXVsdGkuYWdlbnRib3hjcm0uY29tLmF1L2FkbWluLw',
                'X-API-Key' => '1930-3426-4edc-1c98-a09e-910c-a7e0-ed71-1cd9-a753'
            ]
        ]
    );

    if (is_array($response) && !is_wp_error($response)) {
        $headers = $response['headers']; // array of http header lines
        $body    = $response['body']; // use the content
    }
    $body = json_decode($body);

    if ($return_body == true) {
        return $body;
    } else {
        return $body->response->listing->id;
        error_log(print_r($body, true), 0);
    }
}


/**
 * Create Metabox on Property/Rentals for the attached images.
 */
add_action('add_meta_boxes', function () {
    add_meta_box('attached-metabox', __('Attached Images', 'sl'), 'attached_images_render', ['property', 'rental']);
    add_meta_box('epl-dump-metabox', __('Easy Property Listings Data Dump', 'sl'), 'epl_post_dump', ['property', 'rental']);
});

function epl_post_dump($post)
{

    $property_meta = get_post_meta($post->ID);
    $meta_return = '<ul>';
    foreach ($property_meta as $key => $meta) {

        $meta_return .= '<li><h5 style="margin-bottom: 0px;">' . $key . '</h5>';
        foreach ($meta as $meta_item) {
            $meta_return .= $meta_item;
            $meta_return .= '</li>';
        }
    }
    $meta_return .= '</ul>';

    $meta_return .= '
    <style>
    #epl-dump-metabox ul {
        display: flex;
        flex-wrap: wrap;
    }
    #epl-dump-metabox ul li h5 {
        margin: 0px;
    }
    #epl-dump-metabox ul li {
        margin: 0px 15px 10px 15px;
        padding: 5px;
        width: 100%;
        border: 1px solid #f7f7f7;
    }
    #epl-dump-metabox .inside {
        overflow-y: scroll;
        max-height: 500px;
    }
    </style>
    ';

    echo $meta_return;
}
/*
 * Render the metabox
 */
function attached_images_render($post)
{

    //$attached_media = get_attached_media( 'image', $post->ID );
    $render = '<ul>';

    $attached_media = get_posts(array(
        'post_parent' => $post->ID,
        'post_type' => 'attachment',
        'post_mime_type' => 'image',
        'orderby' => 'ID',
        'order' => 'ASC',
        'numberposts' => -1
    ));

    foreach ($attached_media as $s) {

        $render .= '
            <li data-attachment="' . $s->ID . '">
                <label>' . $s->ID . '</label>
                <a href="' . wp_get_attachment_url($s->ID) . '" target="_blank">'
            . wp_get_attachment_image($s->ID, 'medium') .
            '</a>
                <span><strong>Modified:</strong> <br>' . $s->post_modified . '</span><br>
                <span><strong>Filename:</strong> <br> ' . $s->post_name . '</span>
            </li>';
    }
    $render .= '</ul>';
    $render .= '
    <style>
        #attached-metabox ul {
            display: flex;
            flex-wrap: wrap;
        }
        #attached-metabox ul li {
            width: calc(25% - 23px);
            margin-bottom: 15px;
            margin-right: 15px;
            position: relative;
            border: 1px solid #80808017;
            box-shadow: 1px 1px 20px rgba(0,0,0,0.05);
            padding: 3px;
        }
        #attached-metabox ul li img {
            max-width: 100%;
            object-fit: cover;
            max-height: 170px;
            
        }
        #attached-metabox ul li label {
            position: absolute;
            top: 0;
            left: 0;
            padding: 5px;
            background-color: rgba(255,255,255,0.9);
            font-size: 12px;
            font-weight: bold;
        #attached-metabox ul li span {
            display: block;
            font-size: 10px;
            font-weight: 700;
    </style>';

    echo $render;
}

function the_sold_date($prop = '')
{

    $sold_date = '';

    if ($prop['property_status'][0] == 'sold') {

        $sold_date = 'Sold on ' . date('jS \of F Y', strtotime($prop['property_sold_date'][0]));

        if ($prop['property_sold_price_display'][0] == 'yes') {

            $sold_date .= ' for $' . number_format($prop['property_sold_price'][0]);
        }
    } else {

        $sold_date = wp_kses_post(epl_get_property_price());
    }

    echo $sold_date;
}

function more_post_ajax()
{
    $offset = $_POST["offset"];
    $ppp    = $_POST["ppp"];
    header("Content-Type: text/html");

    $author_current_listings = array(
        'post_type'   => 'property',
        'post_status' => 'publish',
        'posts_per_page' => $ppp,
        'offset' => $offset,
        'meta_query' => array(
            'relation' => 'AND',
            array(
                'key' => 'property_status',
                'value' => 'current',
                'compare' => '=',
            ),
            array(
                'relation' => 'OR',
                array(
                    'key' => 'property_agent',
                    'value' => $user->data->user_login,
                    'compare' => '=',
                ),
                array(
                    'key' => 'property_second_agent',
                    'value' => $user->data->user_login,
                    'compare' => '=',
                ),
            )
        )
    );
    oc_property_grid($author_current_listings);

    exit;
}

add_action('wp_ajax_nopriv_more_post_ajax', 'more_post_ajax');
add_action('wp_ajax_more_post_ajax', 'more_post_ajax');

/**
 *  Before You Bid Link.
 *  Add to property page, and it will automatically
 *  generate the correct Link to get a property report
 *  through https://www.beforeyoubid.com.au/
 */
function before_you_bid()
{

    $title          = get_the_title();
    $name           = get_the_author_meta('display_name');
    $author_id      = get_the_author_meta('ID');
    $author_mobile  = get_user_meta($author_id, 'mobile', true);
    $author_email   = get_the_author_meta('user_email');

    $link = 'https://beforeyoubid.com.au/address-map-action/';
    $link .= urlencode(str_replace('/', 'BYBFS', $title));
    $link .= '/OCRESA/';
    $link .= urlencode($name) . ',';
    $link .= urlencode(str_replace('tel:', '', $author_mobile)) . ',';
    $link .= urlencode($author_email);

    return $link;
}

/**
 * Input $args which whill be passed to a WP_Query.
 */
function get_single_paginated_property($ID = '', $strip = false)
{

    $args = [
        'post_type' => 'any',
        'p' => $ID
    ];

    $query = new WP_Query($args);

    if ($query->have_posts()) {

        $properties = '';

        while ($query->have_posts()) {
            $query->the_post();

            $prop = get_post_meta(get_the_ID());

            if (get_post_type(get_the_ID()) == 'property') {
                $price = $prop['property_price_view'][0];
            } elseif (get_post_type(get_the_ID()) == 'rental') {
                $price = '$' . $prop['property_rent'][0] . ' / ' . $prop['property_rent_period'][0];
            }

            if ($prop['property_status'][0] == 'sold' || $prop['property_status'][0] == 'leased') {
                $status = "<span class='status-" . $prop["property_status"][0] . "'>" . $prop["property_status"][0] . "</span>";
            } else {
                $status = "";
            }

            if (get_field('display_street_address')) {
                $street_address = get_field('display_street_address');
            } else {
                $street_address = $prop['property_address_street_number'][0] . ' ' . $prop['property_address_street'][0];
            }

            $type = get_post_type();
            if (has_post_thumbnail()) {
                $img_url = get_the_post_thumbnail_url(get_the_ID(), 'property-grid');
            } else {
                $img_url = get_template_directory_uri() . '/media/placeholder.jpg';
            }

            $features = [
                'Bed' => $prop['property_bedrooms'][0],
                'Bath' => $prop['property_bathrooms'][0],
                'Car' => $prop['property_garage'][0]
            ];

            $features_list = '';
            foreach ($features as $key => $feature) {
                if ($feature != '' && $feature != '0') {
                    $features_list .= "<div class='feature'><span class='display-1 small'>" . $feature . "</span><span class='" . $key . "' alt='" . $key . "' />" . $key . "</span></div>";
                }
            }

            if ($prop['property_status'][0] == 'offmarket') {
                $exclusive = "<span class='oc-exclusive-label'>OC Exclusive</span>";
            } else {
                $exclusive = '';
            }

            $sold_date = '';
            if ($prop['property_status'][0] == 'sold') {
                $sold_date = 'Sold on ' . date('jS \of F Y', strtotime($prop['property_sold_date'][0]));
                if ($prop['property_sold_price_display'][0] == 'yes') {
                    $sold_date .= ' for $' . number_format($prop['property_sold_price'][0]);
                }
            } else {
                $sold_date = wp_kses_post(epl_get_property_price());
            }

            $properties .=
                '<div class="col-xs-1-12 col-md-4 mb-20 property-grid-item">
                <div class="polaroid-property">
                    <a href="' . get_permalink() . '">
                        ' . wp_kses_post(epl_get_price_sticker()) . '
                        <img src="' . $img_url . '" class="property-image" />
                        ' . $exclusive . '
                        <div class="property-content">
                            <div class="property-header">
                                <h5 class="">' . $street_address . '</h5>
                                <p class="suburb">' . $prop['property_address_suburb'][0] . '</p>
                                <p class="caption">' . $prop['property_heading'][0] . '</p>
                            </div>
                            <div class="property-footer">
                                <p class="property-price">' . $sold_date . '</p>
                                <div class="features">' . get_property_features() . '</div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>';

            if ($strip = true) {
                $properties = preg_replace("/\r|\n/", "", $properties);
            }
        }
    }
    wp_reset_postdata();

    return $properties;
}

/**
 * I'm not proud of this one.
 * 
 */
function clean($string)
{
    $string = str_replace('/', '-', $string); // Replaces all spaces with hyphens.
    $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
    $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

    return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
}

function ocre_delete_transients_on_property_import($import_id)
{
    // Only clear transients if the import is a rental or property.
    if ($import_id != "1") return;
    
    // Retrieve the last import run stats.
    global $wpdb;
    $table = $wpdb->prefix . "pmxi_imports";

    if ($soflyyrow = $wpdb->get_row($wpdb->prepare("SELECT * FROM `" . $table . "` WHERE `id` = '%d'", $import_id))) {

        if (($soflyyrow->created > 0) || $soflyyrow->updated > 0) {
            delete_transient('cache_key_property_1');
            delete_transient('cache_key_property_sold');
            delete_transient('cache_key_property_current');
            delete_transient('cache_key_property_');
            
            delete_transient('get_suburbs_with_count_property_current');
            delete_transient('get_suburbs_with_count_property_sold');
            error_log('Properties have updates. Clearing transients', 0);

        }
    }
}

add_action('pmxi_after_xml_import', 'ocre_delete_transients_on_property_import', 10, 1);

function property_rates()
{

    if (have_rows('specs')) {

        $specs = get_field('specs');

        $return = '<h4 class="tab-title">Outgoings</h4><ul class="epl-property-features">';

        while (have_rows('specs')) {

            the_row();

            foreach ($specs as $key => $spec) {

                $sub_field = get_sub_field_object($key);

                if (!empty($sub_field['value'])) {

                    if (strpos($sub_field['label'], 'Period') !== false) {
                        $return .= $sub_field['value'] . '</span></li>';
                    } else {
                        $return .= '<li><strong>' . $sub_field['label'] . '</strong> <span>$' . $sub_field['value'] . ' per ';
                    }
                }
            }
        }
    }
    $return .= '</ul>';
    return $return;
}

add_shortcode('ocre_grid', 'ocre_property_grid_shortcode');
function ocre_property_grid_shortcode($args) {
    $defaults = [
        'post_type'         => 'property',
        'property_status' => 'current',
        'posts_per_page'    => 3,
		'agentbox_unique_id' => '',
    ];

    $args = wp_parse_args( $args, $defaults );

    if(!empty($args['agentbox_unique_id'])) { 
        $propertyIDS = explode(',', $args['agentbox_unique_id']);
    } else {
        $propertyIDS = '';
    }

    $current_args = [
        'post_type'         => $args['post_type'],
        'order'             => 'DESC',
        'orderby'           => 'date',
        'paged'             => false,
        'posts_per_page'    => $args['posts_per_page'],
    ];
    if (!empty($args['agentbox_unique_id'])) {
       $meta = 
            [
                'key'     => 'agentbox_unique_id',
                'value'   => $propertyIDS,
                'compare' => 'IN'
            ];
            $current_args['meta_query'] = $meta;
    }

    return oc_property_grid($current_args);; 
}


// Change epl garage field to car garage on single listing
function my_edited_property_garage($field)
{
    $field['label'] = __('Car Garage', 'epl');
    return $field;
}
add_filter('epl_meta_property_garage', 'my_edited_property_garage');


add_action('gform_after_submission_6', 'post_agent_enquiry_to_agentbox', 10, 2);
function post_agent_enquiry_to_agentbox($entry)
{
    $contact = new ocre\agentbox_contact($entry);
}

function get_suburbs_with_count($post_type = 'property', $status = 'current' ) {

    $transient_key = 'get_suburbs_with_count_' . $post_type . '_' . $status;
    $has_data      = get_transient( $transient_key );

    if ( false === $has_data ) {
        
	    $suburbs = get_terms('location', [ 'post_type' => [ $post_type ], 'fields' => 'all' ]);

	    foreach ( $suburbs as $suburb ) {
        
	    	$status_args = new WP_Query([
	    		'post_type' => $post_type,
	    		'tax_query' => [
	    			[
	    				'taxonomy' => 'location',
	    				'field' => 'id',
	    				'terms' => $suburb->term_id,
	    			]
	    		],
	    		'meta_query' => [
	    			[
	    				'key'     => 'property_status',
	    				'value'   => $status,
	    				'compare' => 'LIKE',
	    			]
	    		]
	    	]);

	    	$suburb->count = $status_args->found_posts;
        
	    	wp_reset_postdata();
	    }

	    $select = '<select data-style="btn-outline-secondary" title="Search by suburb..." name="property_location[]" class="selectpicker form-control" data-live-search="true" data-width="100%" multiple>';
    
	    foreach ( $suburbs as $suburb ) {
	    	if ( $suburb->count <= 0 ) continue;
	    	$select .= '<option data-tokens="' . $suburb->term_id . '" value="' . $suburb->term_id . '">' . $suburb->name . ' <span class="caption">(' . $suburb->count . ')</span></option>';
	    }

	    $select .= '</select>';

        set_transient($transient_key, $select, 24 * HOUR_IN_SECONDS);

    } else {
        $select = get_transient( $transient_key );
    }

	return $select;

}

function dont_import_duplicates( $continue_import, $data, $import_id ) {

    global $wpdb;

    $unique_ID = $wpdb->get_var( $wpdb->prepare( "SELECT post_id FROM $wpdb->postmeta WHERE meta_key='property_unique_id' AND meta_value='%s' LIMIT 1", $data['uniqueID'] ) );
	
    if ( $unique_ID ) {
        // property already exists, delete it, then import.
        var_dump('duplicate found. removing');
        wp_delete_post($unique_ID, true);
        
        return true;
        
    } else {
        var_dump('No duplicates found. Importing property.');
        return true;

    }
}

add_filter('wp_all_import_is_post_to_create', 'dont_import_duplicates', 10, 3);