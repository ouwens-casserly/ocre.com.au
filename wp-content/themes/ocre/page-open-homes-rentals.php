<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package UnderStrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$page_for_posts = get_option( 'page_for_posts' ); 
if ( has_post_thumbnail($page_for_posts) ) {
    $thumb_id = get_post_thumbnail_id( $page_for_posts);
    $url = wp_get_attachment_url( $thumb_id );
} else {
    $url = '';
}

$rental_open_homes = [
	'post_type'         => 'rental',
	'order'             => 'DESC',
	'orderby'           => 'date',
	'posts_per_page'    => 21,
	'meta_query' => [
		'relation' => 'AND',
		[
			'key'     => 'property_inspection_times',
			'compare' => '!=',
			'value' => '',
		],
		[
			'key'     => 'property_status',
			'value'   => 'current',
			'compare' => 'LIKE',
		]

	]
];

/**
 * Use Easy Property listings to get all the suburbs with properties in them.
 */
if (function_exists('epl_get_available_locations')) {

	$locations = epl_get_available_locations('property', 'current');
	
	$location_array = [];
	foreach ($locations as $location ) {
		$location_array[] = $location;
	}

}

?>

<div class="wrapper" id="page-wrapper">

    <div class="" style="background-image: url(<?php echo $url; ?>); background-size: cover; padding-top: 170px; padding-bottom: 140px; background-position: center;">

        <div id="hero" class="container">

          <div class="row">

            <div class="col-md-8 offset-md-2 text-center">

                <h1 class="display-1 text-light font-italic">Open Homes For <span>Lease</span></h1>

                <p class="subheading-1 text-light">We invite you to check out the range of Adelaide open inspections coming up. If you are looking at renting in South Australia, then our experienced property managers can help you.</p>

                <a href="/digital-appraisal/" class="btn btn-outline-light">Get an appraisal</a>
            </div>

        </div>

      </div>

    </div>

	<div class="container mt-120" id="content" tabindex="-1">

		<div class="row">

			<!-- Do the left sidebar check -->
			<?php get_template_part( 'global-templates/left-sidebar-check' ); ?>

			<main class="site-main" id="main">

				<?php while ( have_posts() ) { the_post(); } ?>

				<div class="row">
				
					<?php oc_property_grid($rental_open_homes); ?>
				
				</div>
            
			</main><!-- #main -->

			<!-- Do the right sidebar check -->
			<?php get_template_part( 'global-templates/right-sidebar-check' ); ?>

		</div><!-- .row -->

	</div><!-- #content -->

	<div class="container mt-90">

	<!-- <div class="row">

		<div class="col-md-8">

			<h2 class="mb-0">Testimonials</h2>
			<p class="subheader mb-4">See what our clients are saying</p>
					
		</div>

		<div class="col-md-4">
			<a href="https://www.ratemyagent.com.au/real-estate-agency/ouwens-casserly-real-estate-ak213/sales/reviews" class="btn btn-outline-primary btn-sm float-right" target="_blank">View all on Rate My Agent</a>
		</div>

	</div> -->
		
		<!-- <div class="row pb-120">

			<div class="col-md-12">

				<?php //echo do_shortcode('[rmaa_slider]'); ?>

			</div>

		</div> -->

	</div>

	<?php get_template_part( 'global-templates/footer-cta' ); ?>	

</div><!-- #page-wrapper -->

<script>

$ = jQuery;
var locations =<?php echo json_encode($location_array );?>;
$(function() {
    autocomplete(document.getElementById("property_address"), locations);
});

</script>
<?php
get_footer();
