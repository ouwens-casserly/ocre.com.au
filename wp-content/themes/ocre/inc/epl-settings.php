<?php

/**
 * Hooks, functions, & settings to directly affect Easy Property Listings functionality.
 * 
 */
add_filter('epl_sorting_options', 'add_sort_by_sold_date');

function add_sort_by_sold_date($sort_orders) {
    $sort_orders[] = array(				
        'id'      => 'sold_latest',
        'label'   => __( 'Sold Date: Latest', 'easy-property-listings' ),
        'type'    => 'meta',
        'key'     => 'property_sold_date',
        'order'   => 'DESC',
        'orderby' => 'meta_value',
    );

    $sort_orders[] = array(				
        'id'      => 'sold_oldest',
        'label'   => __( 'Sold Date: Oldest', 'easy-property-listings' ),
        'type'    => 'meta',
        'key'     => 'property_sold_date',
        'order'   => 'ASC',
        'orderby' => 'meta_value',
    );

    return $sort_orders;
}


function output_the_thing($return) {

    if ( $return == '<li class="open_spaces">Open Car Parking Spaces</li>' ) {
        $return = '<li class="open_spaces">1 Open Car Parking Space</li>';
    }
    return $return;
}

add_filter('epl_get_additional_features_html', 'output_the_thing');

//change the label for open spaces to open parking spaces.
function open_space_edited($field) {
	$field['label'] = __('Open Car Parking Spaces','epl');
	return $field;
}
add_filter('epl_meta_property_open_spaces','open_space_edited');

//change the label for open spaces to open parking spaces.
function total_parking_spaces($total) {
    global $property;
    $open_parking = $property->get_property_meta('property_open_spaces');
    $total = $total + $open_parking;
    return $total;

}
add_filter('epl_total_parking_spaces','total_parking_spaces');


function my_parking($parking) {
    $parking = str_replace('Parking Spaces', 'Total Parking Spaces', $parking);
    return $parking;
}
add_filter('epl_get_property_parking', 'my_parking');

add_filter('epl_property_general_features_list', 'modify_property_features');

function modify_property_features($features) { 

    $index = array_search('category', $features);
    if( $index !== false ) { unset( $features[$index] ); }

    $index = array_search('bed', $features);
    if( $index !== false ) { unset( $features[$index] ); }
    
    $index = array_search('bath', $features);
    if( $index !== false ) { unset( $features[$index] ); }

    $index = array_search('rooms', $features);
    if( $index !== false ) { unset( $features[$index] ); }

    return $features;
}

function epl_fl_property_category($array) {
	$array = array(
		'House'  		=>	__('House', 'epl'),
		'Unit'			=>	__('Unit', 'epl'),	
		'Townhouse'		=>	__('Townhouse', 'epl'),	
		'Villa'			=>	__('Villa', 'epl'),	
		'Apartment'		=>	__('Apartment', 'epl'),	
		'Flat'			=>	__('Flat', 'epl'),	
		'Studio'		=>	__('Studio', 'epl'),	
		'DuplexSemi-detached'	=>	__('Duplex Semi-detached', 'epl'),
		'AcreageSemi-rural'	=>	__('Acreage Semi-rural', 'epl'),
		'Retirement'		=>	__('Retirement', 'epl'),	
		'BlockOfUnits'		=>	__('Block Of Units', 'epl'),	
		'ServicedApartment'	=>	__('Serviced Apartment', 'epl'),
		'Other'			=>	__('Other', 'epl'),
		'Land'			=> __('Land', 'epl')
	);
	return $array;
}
add_filter('epl_listing_meta_property_category', 'epl_fl_property_category');
 