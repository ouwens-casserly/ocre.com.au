<?php
/**
 * Custom hooks
 *
 * @package UnderStrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

if ( ! function_exists( 'ocre_site_info' ) ) {
	/**
	 * Add site info hook to WP hook library.
	 */
	function ocre_site_info() {
		do_action( 'ocre_site_info' );
	}
}

add_action( 'ocre_site_info', 'ocre_add_site_info' );
if ( ! function_exists( 'ocre_add_site_info' ) ) {
	/**
	 * Add site info content.
	 */
	function ocre_add_site_info() {
		$the_theme = wp_get_theme();

		$site_info = 'RLA 275403 | RLA 286513 | RLA 268527 | RLA 304568 | RLA 276489 | RLA 223245 | RLA 281321 ';

		echo apply_filters( 'ocre_site_info_content', $site_info ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
	}
}
