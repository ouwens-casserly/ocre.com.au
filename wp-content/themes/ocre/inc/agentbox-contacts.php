<?php

namespace ocre;

/**
 * Send form submissions to agentbox via their API.
 * This fires after someone submits a gravity form submission.
 *
 *  $entry['source_url'] The URL the form was submitted on.
 *  $entry['1'] First and last name of the user
 *  $entry['2'] user email address
 *  $entry['3'] user phone number
 *  $entry['4'] User message
 *  $entry['5'] How did you hear about us
 *  $entry['6'] Property Unique ID
 *  $entry['8'] Agent Name
 *  $entry['9'] Agent Email address
 *  $entry['10'] Agent Secondary email address
 */




class agentbox_contact
{
    /**
     * Define the API key and headers.
     * @param array
     */
    private $headers = [
        'Content-Type' => 'application/json',
        'Accept' => 'application/json',
        'X-Client-ID' => 'aHR0cHM6Ly9vY3JlbXVsdGkuYWdlbnRib3hjcm0uY29tLmF1L2FkbWluLw',
        'X-API-Key' => '1930-3426-4edc-1c98-a09e-910c-a7e0-ed71-1cd9-a753'
    ];

    /**
     * Set the endpoint for the Agentbox API.
     * @param string
     */
    private $url = 'https://api.agentboxcrm.com.au/';

    /**
     *  Set the headers, method, etc.
     * @param array
     */
    private $options;

    /**
     *  Splits the first and last name from the entry into an array.
     * @param array
     */
    private $name;

    private $agent_id;

    private $contact_id;
    /**
     *  Enquiries endpoint. Allows for creation of a enquiry. 
     *  It can be used to post a general enquiry, or a listing/project enquiry.
     * @param array
     */
    private $enquiries = 'enquiries?version=2';

    function __construct($entry)
    {
        $this->entry = $entry;
        $this->agent_id = $this->get_agent_id();
        $this->name = explode(' ', $entry['1']);
        if($this->name[1] == '') $this->name[1] = 'none supplied';
        $this->options = [
            'headers' => $this->headers,
            'method' => 'POST',
            'data_format' => 'body'
        ];
        $this->$contact_id = $this->get_contact_id();

    }

    /**
     * Takes the agents' email address and returns their Agentbox ID.
     * @return array|false
     */
    public function get_agent_id()
    {
        error_log('searching for agent...', 0);
        $query = 'staff?page=1&limit=1&filter[email]=' . urlencode($this->entry['9']) . '&version=2';
        $response = wp_remote_get($this->url . $query, ['headers' => $this->headers]);

        if (is_array($response) && !is_wp_error($response) && $response['response']['code'] == '200') {
            $return = json_decode($response['body']);
            error_log('Agent found.', 0);
            return $return->response->staffMembers[0]->id;
        } else {
            error_log('could not find agent. Enquiry will not be sent to Agentbox.', 0);
            return false;
        }
    }

    /**
     * Takes the users' email address and returns their Agentbox ID.
     * @return array|false
     */
    public function get_contact_id()
    {
        error_log('searching for contact...', 0);
        $query = 'contacts?page=1&limit=1&filter[email]=' . urlencode($this->entry['2']) . '&filter[matchAllContactClass]=false&filter[reqIncSurroundSuburbs]=false&filter[limitSearch]=true&version=2';
        $response = wp_remote_get($this->url . $query, ['headers' => $this->headers]);

        if (is_array($response) && !is_wp_error($response)) {

            $response = json_decode($response['body'], true);

            if ($response['response']['items'] >= 1 && $response['response']['contacts'][0]['id'] != false ) {
                error_log('contact found.', 0);
                $this->create_entry();
                return $response['response']['contacts'][0]['id'];
            }

            if($response['response']['items'] == '0') {
                $this->add_contact();
                return false;
            }
        }
    }
    
    /**
     * Adds a contact into the Agentbox system. Only to be used if the email
     * of the user cannot be found.
     * @return array|false
     */
    public function add_contact()
    {
        error_log('Contact not found. Adding contact to Agentbox...', 0);
        $body = [
            "contact" => [
                "firstName" => $this->name[0],
                "lastName" => $this->name[1],
                "type" => "Person",
                "email" => $this->entry['2'],
                "mobile" => $this->entry['3'],
                "comments" => $this->entry['4'],
                "attachedRelatedStaffMembers" => [
                    [
                        "id" => $this->agent_id
                    ]
                ]
            ]
        ];
        $this->options['body'] = wp_json_encode($body);

        $response = wp_remote_post($this->url . 'contacts?version=2', $this->options);

        if (is_array($response) && !is_wp_error($response) && $response['response']['code'] == '201') {
            error_log('Contact created.', 0);
            $this->create_entry();
        } else {
            error_log('Contact could not be created. Message was not sent to agent.', 0);
            error_log(print_r($response['body'], true), 0);
            return false;
            
        }
    }

    /**
     * Update the contact in Agentbox.
     * @return string|false
     */
    public function update_contact()
    {
        // Coming soon.
        return false;
    }

    /**
     * Get the property ID of the existing property.
     * @return string|false
     */
    public function get_property_id()
    {
        // Coming soon.
        return false;
    }

    /**
     * Create the enquiry in Agentbox.
     * @return array|false
     */
    public function create_entry()
    {

        $body = [
            "enquiry" => [
                "comment" => $this->entry['4'],
                "source" => "website",
                "attachedContact" => [
                    "id" => $this->$contact_id,
                    "firstName" => $this->name[0],
                    "lastName" => $this->name[1],
                    "email" => $this->entry['2']
                ],
                "attachedListing" => [
                    "id" => $this->entry['6']
                ]
            ]
        ];

        $this->options['body'] = wp_json_encode($body);

        $response = wp_remote_post($this->url . $this->enquiries, $this->options);

        if (is_array($response) && !is_wp_error($response) && $response['response']['code'] == '201') {
            error_log('Message sent to Agentbox successsfully.', 0);
            return true;
        } else {
            error_log('Could not create entry: ' . print_r($response['body'], true), 0);
            return false;
        }
        
    }

}