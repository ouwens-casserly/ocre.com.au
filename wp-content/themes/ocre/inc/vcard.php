<?php

/**
 * Determine that url is exists or not
 *
 * @param $url = The url to check
 **/
function url_exists($url) {
    $result = false;
    $url = filter_var($url, FILTER_VALIDATE_URL);
    
    /* Open curl connection */
    $handle = curl_init($url);
    
    /* Set curl parameter */
    curl_setopt_array($handle, array(
        CURLOPT_FOLLOWLOCATION => TRUE,     // we need the last redirected url
        CURLOPT_NOBODY => TRUE,             // we don't need body
        CURLOPT_HEADER => FALSE,            // we don't need headers
        CURLOPT_RETURNTRANSFER => FALSE,    // we don't need return transfer
        CURLOPT_SSL_VERIFYHOST => FALSE,    // we don't need verify host
        CURLOPT_SSL_VERIFYPEER => FALSE     // we don't need verify peer
    ));

    /* Get the HTML or whatever is linked in $url. */
    $response = curl_exec($handle);
    
    $httpCode = curl_getinfo($handle, CURLINFO_EFFECTIVE_URL);  // Try to get the last url
    $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);      // Get http status from last url
    
    /* Check for 200 (file is found). */
    if($httpCode == 200) {
        $result = true;
    }
    
    return $result;
    
    /* Close curl connection */
    curl_close($handle);
}

function image_encode_url($path) {

    if ( url_exists($path) == true ) {
        $context  = stream_context_create([
            "ssl" => [
                "verify_peer" => false,
                "verify_peer_name" => false,
            ],
        ]);
    
        $image = file_get_contents( $path, false, $context );
        $finfo = new finfo(FILEINFO_MIME_TYPE);
        $type  = $finfo->buffer($image);
        return "PHOTO;TYPE=".$type.";ENCODING=BASE64:".base64_encode($image);
    } else {
        return false;
    }

}

/**
 * Create the Vcard.
 *
 * @param $author = The ID of the author you want to generate a vcard for.
 **/
function generate_vcard( $author ) {

    $author_meta = get_user_meta($author);
    $filepath = get_template_directory()."/vcard/".$author.'.vcf';
    $ocre_vcard_created_time = get_user_meta($author, 'ocre_vcard_created_time');
    $ocre_profile_updated = get_user_meta($author, 'ocre_profile_updated');

    if( !file_exists($filepath) || $ocre_vcard_created_time[0] < $ocre_profile_updated[0] ) {

        $author_custom_meta = get_fields('user_'. $author);
        $email = get_the_author_meta('user_email', $author);
        $office = 'Ouwens Casserly ' . get_the_title( $author_custom_meta['office'][0] );
        $office_address = get_field('address', $author_custom_meta['office'][0]);
        $url = get_author_posts_url(get_the_author_meta($author)).get_the_author_meta('user_nicename',$author);
        $image = $author_custom_meta['static_profile_image']['sizes']['medium_large'];

        $today = date('Y-m-d'); 
        $mobile = str_replace('tel:', '', $author_meta['mobile'][0]);
        $data = null;
        $data .= "BEGIN:VCARD\n";
        $data .= "VERSION:3.0\n";
        $data .= "REV:".$today."T00:00:00Z\n";
        $data .= "N:" .$author_meta['last_name'][0].";" .$author_meta['first_name'][0].";;;\n";
        $data .= "ORG:" .$organisation."\n";
        $data .= "TITLE:" .$job_title."\n";
        $data .= "EMAIL;INTERNET;WORK:" .$email."\n";
        $data .= "TEL;WORK:" .$mobile."\n";
        $data .= "ADR;WORK:" .$office_address['address']."\n";
        $data .= "URL;WORK:" .$url."\n";

        if( !empty( $image ) ) {
            $data .= "" .image_encode_url($image)."\n";
        }

        $data .= "END:VCARD";

        $file = fopen($filepath, 'w');
        fwrite($file, $data);
        fclose($file);
        update_user_meta( $author, 'ocre_vcard_created_time', current_time( 'mysql' ) );
        return true;
    }
    return true;

}

/**
 * Add user meta to check if they have been updated before generating the vcard.
 */
function update_user_modified_time( $user_id ) {
    update_user_meta( $user_id, 'ocre_profile_updated', current_time( 'mysql' ) );
}
add_action( 'profile_update', 'update_user_modified_time' );