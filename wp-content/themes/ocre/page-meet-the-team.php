<?php

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package UnderStrap
 */

// Exit if accessed directly.
defined('ABSPATH') || exit;

get_header();

$container = get_theme_mod('understrap_container_type');
$post = get_post();
$featured_img_url = get_the_post_thumbnail_url($post->ID, 'full');

?>

<div class="wrapper" id="page-wrapper">

	<div id="team-header" class="container mt-90 mb-90">

		<div class="row">

			<div class="col-md-6 offset-md-3 text-center">

				<h1 class="display-1 italic">Meet the <span>Ouwens Casserly</span> Team</h1>

				<p>Our team of Adelaide real estate experts can help achieve your property goals. Let's make it happen. </p>

			</div>

		</div>

		<div class="row">

			<div class="col-md-6 offset-md-3">

				<div class="user-search form-group">
					<input type="text" name="keyword" id="keyword" onkeyup="fetch()" placeholder="Find an expert..." class="form-control"></input>
					<div id="datafetch" class="staff-search-results"></div>
				</div>

			</div>

		</div>

	</div>



	<div class="wrapper" id="page-wrapper">

		<div class="<?php echo esc_attr($container); ?>" id="content" tabindex="-1">

			<div class="row">

				<?php get_template_part('global-templates/left-sidebar-check'); ?>

				<main class="site-main" id="main">

					<?php while (have_posts()) : the_post();
						the_content(); ?>

						<ul class="nav nav-pills" id="rolesTab" role="tablist">

							<li class="nav-item" role="presentation">
								<a class="nav-link active show" id="all-tab" data-toggle="pill" href="#all" role="tab" aria-controls="all" aria-selected="true">Our Team</a>
							</li>

							<li class="nav-item" role="presentation">
								<a class="nav-link" id="directors-tab" data-toggle="pill" href="#directors" role="tab" aria-controls="directors" aria-selected="true">Directors</a>
							</li>

							<li class="nav-item dropdown parent">
								<a class="nav-link dropdown-toggle" id="subDropDownMenu" data-toggle="dropdown" href="#" role="tab" aria-expanded="false" aria-selected="false">Sales</a>
								<div class="dropdown-menu" aria-labelledby="subDropDownMenu">
									<a class="dropdown-item" data-toggle="pill" href="#all-sales" role="tab" aria-controls="all-sales" aria-selected="false">All Sales</a>
									<a class="dropdown-item" data-toggle="pill" href="#adelaide" role="tab" aria-controls="adelaide" aria-selected="false">Adelaide</a>
									<a class="dropdown-item" data-toggle="pill" href="#henley-beach" role="tab" aria-controls="henley-beach">Henley Beach</a>
									<a class="dropdown-item" data-toggle="pill" href="#kensington" role="tab" aria-controls="kensington">Kensington</a>
									<a class="dropdown-item" data-toggle="pill" href="#unley" role="tab" aria-controls="unley">Unley</a>
									<a class="dropdown-item" id="willunga-tab" data-toggle="pill" href="#willunga" role="tab" aria-controls="willunga">Willunga</a>
								</div>
							</li>

							<li class="nav-item" role="presentation">
								<a class="nav-link" id="pm-tab" data-toggle="pill" href="#pm" role="tab" aria-controls="pm" aria-selected="false">Property Management</a>
							</li>

							<li class="nav-item" role="presentation">
								<a class="nav-link" id="projects-tab" data-toggle="pill" href="#projects" role="tab" aria-controls="projects" aria-selected="false">Project Development</a>
							</li>

							<li class="nav-item" role="presentation">
								<a class="nav-link" id="corporate-tab" data-toggle="pill" href="#central-support" role="tab" aria-controls="corporate" aria-selected="false">Central Support</a>
							</li>

						</ul>

						<div class="tab-content" id="rolesTabContent">

							<div class="tab-pane fade show active" id="all" role="tabpanel" aria-labelledby="home-tab">
								<?php oc_team_list('', 'col-md-3 col-lg-3 col-6', []); ?>
							</div>

							<div class="tab-pane fade show" id="directors" role="tabpanel" aria-labelledby="home-tab">
								<?php oc_team_list('directors', 'col-md-3 col-lg-3 col-6', ['3058']); ?>
							</div>

							<div class="tab-pane fade show" id="all-sales" role="tabpanel" aria-labelledby="all-sales-tab">
								<?php oc_team_list('sales', 'col-md-3 col-lg-3 col-6', ['3058', '3060', '3061', '3063', '3059']); ?>
							</div>

							<div class="tab-pane fade" id="adelaide" role="tabpanel" aria-labelledby="adelaide-tab">
								<?php oc_team_list('sales', 'col-md-3 col-lg-3 col-6', '3058'); ?>
							</div>

							<div class="tab-pane fade" id="henley-beach" role="tabpanel" aria-labelledby="henley-beach-tab">
								<?php oc_team_list('sales', 'col-md-3 col-lg-3 col-6', '3060'); ?>
							</div>

							<div class="tab-pane fade" id="kensington" role="tabpanel" aria-labelledby="kensington-tab">
								<?php oc_team_list('sales', 'col-md-3 col-lg-3 col-6', '3061'); ?>
							</div>

							<div class="tab-pane fade" id="unley" role="tabpanel" aria-labelledby="unley-tab">
								<?php oc_team_list('sales', 'col-md-3 col-lg-3 col-6', '3063'); ?>
							</div>

							<div class="tab-pane fade" id="willunga" role="tabpanel" aria-labelledby="willunga-tab">
								<?php oc_team_list('sales', 'col-md-3 col-lg-3 col-6', '3059'); ?>
							</div>

							<div class="tab-pane fade" id="pm" role="tabpanel" aria-labelledby="profile-tab">
								<?php oc_team_list('property_management', 'col col-sm-6 col-md-6 col-lg-3'); ?>
							</div>

							<div class="tab-pane fade" id="projects" role="tabpanel" aria-labelledby="projects-tab">
								<?php oc_team_list('projects', 'col-md-3 col-lg-3 col-6', '3120'); ?>
							</div>

							<div class="tab-pane fade" id="central-support" role="tabpanel" aria-labelledby="contact-tab">
								<?php oc_team_list('corporate', 'col-md-3 col-lg-3 col-6'); ?>
							</div>
						</div>

					<?php endwhile; ?>

				</main><!-- #main -->

				<!-- Do the right sidebar check -->
				<?php get_template_part('global-templates/right-sidebar-check'); ?>

			</div><!-- .row -->

		</div><!-- #content -->

		<div class="inner-wrapper pt-90 pb-90 mt-5">
			<div class="container">
				<div class="row mb-5">
					<div class="col-md-6 text-center offset-md-3">
						<h2 class="display-2 italic">We are here to <span>help.</span></h2>
						<p>We have one of the largest networks of independently owned real estate offices in South Australia, which gives our sellers the edge in accessing the largest cross-council customer base in SA, and gives buyers choice and variety they need to find their perfect home.
						<p>
						<div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups text-center">
							<div class="btn-group mr-2" role="group" aria-label="First group">
								<a href="/digital-appraisal" type="button" class="btn btn-outline-primary btn-sm">Get an instant appraisal</a>

							</div>
							<div class="btn-group mr-2" role="group" aria-label="Second group">
								<a href="/contact-us/" type="button" class="btn btn-link btn-sm">Drop us a line</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div><!-- #page-wrapper -->
<script type="text/javascript">
	function fetch() {

		jQuery.ajax({
			url: '<?php echo admin_url('admin-ajax.php'); ?>',
			type: 'post',
			data: {
				action: 'data_fetch',
				keyword: jQuery('#keyword').val()
			},
			success: function(data) {
				jQuery('#datafetch').html(data);
			}
		});

	}
	$ = jQuery;
	$(function() {


		// Javascript to enable link to tab
		var hash = location.hash.replace(/^#/, ''); // ^ means starting, meaning only match the first hash
		console.log(hash);
		if (hash) {
			$('.nav-pills a[href="#' + hash + '"]').tab('show');
		}

		// Change hash for page-reload
		$('.nav-pills a').on('shown.bs.tab', function(e) {
			window.location.hash = e.target.hash;
		});
	});
</script>
<?php
get_footer();
