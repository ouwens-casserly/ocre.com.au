<?php

/**
 * This search form is displayed on 'properties-for-sale', 'properties-for-rent', 
 * and on the archive-listing.php template. The archive template is used for 'property' and 'rental'
 * post types. 
 * 
 * 	$post_type			The current archive type. 'property' or 'rental'.
 * 	$status_label		'Leased' or 'Sold'
 * 
 */

$postID = get_queried_object_id();

// First check what our post type is. and set it if it's a page.
if ( is_archive() ) {
	$post_type = get_post_type();
} else {
	switch ( $postID ) {
		case 16 : 
			$post_type = 'rental';
			break;
		case 14 : 
			$post_type = 'property';
			break;
		default :
			$post_type = 'property';
			break;
	}
}

if ( $post_type == 'property') $status_label = 'Sold';
if ( $post_type == 'rental') $status_label = 'Leased';

// Check if we're setting a status, otherwise, set it as 'current'.
if( isset($_GET['property_status'] ) ) {
	$status = strtolower($_GET['property_status']);
} else {
	$status = 'current';
}


?>

<div id="form-container">

	<!-- Buttons above search box -->
	<ul class="nav nav-pills">
		<li id="buy-link" class="nav-item">
			<a class="nav-link <?php if ($post_type == 'property' && !is_archive()) { echo 'active'; } ?>" href="/properties-for-sale">Buy</a>
		</li>
		<li id="rent-link" class="nav-item">
			<a class="nav-link <?php if ($post_type == 'rental' && !is_archive()) { echo 'active'; } ?>" href="/properties-for-rent">Rent</a>
		</li>
		<li id="oc-projects-link" class="nav-item">
			<a class="nav-link" href="https://www.ocprojects.com.au/projects-blogs/project/" target="_blank">Off the plan</a>
		</li>
		<li id="sold-leased-link" class="nav-item">
			<a class="nav-link <?php if ($status != 'current') { echo 'active'; } ?>" href="/<?php echo $post_type; ?>/?action=epl_search&post_type=<?php echo $post_type; ?>&property_status=<?php echo $status_label; ?>"><?php echo $status_label; ?></a>
		</li>
		<li id="estimate-link" class="nav-item">
			<a class="nav-link" href="/digital-appraisal">Property estimate</a>
		</li>
		<li id="agents-link" class="nav-item">
			<a class="nav-link" href="/about-us/meet-the-team/#sales">Find an agent</a>
		</li>
		<li id="contact-link" class="nav-item">
			<a class="nav-link" href="/contact-us">Contact us</a>
		</li>
		<li id="buyer-alerts-link" class="nav-item">
			<a href="#buyer-alerts" data-toggle="modal" class="nav-link btn-sm btn btn-outline-primary">Buyer Alerts</a>
		</li>
	</ul>
	<!-- End buttons-->

	<form autocomplete="off" id="property-search-form" method="get" action="<?php echo get_site_url(); ?>/<?php echo $post_type; ?>">
		<div class="form-inline">
			<!-- EPL fields -->
			<input type="hidden" name="action" value="epl_search">
			<input type="hidden" class="post_type" name="post_type" id="post_type" value="<?php echo $post_type; ?>">
			<input type="hidden" class="property_status" name="property_status" id="property_status" value="<?php echo $status; ?>">

			<div id="address_search" class="form-group" style="min-height: 38px;">
				<?php echo get_suburbs_with_count($post_type, $status); ?>
			</div>

			<!-- Button trigger modal -->
			<div class="form-group">
				<button id="search-filters" type="button" class="btn btn-outline-secondary btn-sm" data-toggle="modal" data-target="#exampleModal"> <i class="fa fa-sliders" aria-hidden="true"></i> Filters </button>
			</div>

			<div class="form-group submit-group">
				<button id="PrimarySubmit" name="submit" type="submit" class="btn btn-primary btn-sm"><i class="fa fa-search" aria-hidden="true"></i> Search</button>
			</div>
		</div>

		<!-- Modal -->
		<div class="modal" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Refine your property search</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<label for="property_category">Property Types</label>
							<div>
								<select id="property_category" name="property_category" class="custom-select" multiple="multiple" data-style="btn-outline-seconary">
									<option value="">Any</option>
									<option value="Unit">Unit</option>
									<option value="Apartment">Apartment</option>
									<option value="House">House</option>
									<option value="Townhouse">Townhouse</option>
									<option value="Land">Land</option>
								</select>
							</div>
						</div>

						<div class="form-row">
							<div class="form-group col-md-6">
								<label for="property_price_from">Price (min)</label>
								<div>
									<select id="property_price_from" name="property_price_from" class="custom-select" data-style="btn-outline-seconary">
										<?php if ($post_type == 'property') {
											get_template_part('global-templates/property-value');
										} ?>
										<?php if ($post_type == 'rental') {
											get_template_part('global-templates/rental-value');
										} ?>
									</select>
								</div>
							</div>
							<div class="form-group col-md-6">
								<label for="property_price_to">Price (max)</label>
								<div>
									<select id="property_price_to" name="property_price_to" class="custom-select" data-style="btn-outline-seconary">
										<?php if ($post_type == 'property') {
											get_template_part('global-templates/property-value');
										} ?>
										<?php if ($post_type == 'rental') {
											get_template_part('global-templates/rental-value');
										} ?>
									</select>
								</div>
							</div>
						</div>

						<div class="form-row">
							<div class="form-group col-md-12">
								<div class="form-row">
									<div class="col-md-12">
										<label for="property_bedrooms_min">Bedrooms</label>
									</div>
								</div>
								<div class="btn-group btn-group-toggle" data-toggle="buttons">
									<label class="btn btn-outline-primary">
										<input type="radio" value="1" name="property_bedrooms_min" id="option1" autocomplete="off">1+
									</label>
									<label class="btn btn-outline-primary">
										<input type="radio" value="2" name="property_bedrooms_min" id="option2" autocomplete="off">2+
									</label>
									<label class="btn btn-outline-primary">
										<input type="radio" value="3" name="property_bedrooms_min" id="option3" autocomplete="off">3+
									</label>
									<label class="btn btn-outline-primary">
										<input type="radio" value="4" name="property_bedrooms_min" id="option4" autocomplete="off">4+
									</label>
									<label class="btn btn-outline-primary">
										<input type="radio" value="5" name="property_bedrooms_min" id="option5" autocomplete="off">5+
									</label>
								</div>
							</div>
						</div>

						<div class="form-row">
							<div class="form-group col-md-12">
								<div class="form-row">
									<div class="col-md-12">
										<label for="property_bathrooms">Bathrooms</label>
									</div>
								</div>
								<div class="btn-group btn-group-toggle" data-toggle="buttons">
									<label class="btn btn-outline-primary">
										<input type="radio" value="1" name="property_bathrooms" id="bath1" autocomplete="off">1+
									</label>
									<label class="btn btn-outline-primary">
										<input type="radio" value="2" name="property_bathrooms" id="bath2" autocomplete="off">2+
									</label>
									<label class="btn btn-outline-primary">
										<input type="radio" value="3" name="property_bathrooms" id="bath3" autocomplete="off">3+
									</label>
									<label class="btn btn-outline-primary">
										<input type="radio" value="4" name="property_bathrooms" id="bath4" autocomplete="off">4+
									</label>
									<label class="btn btn-outline-primary">
										<input type="radio" value="5" name="property_bathrooms" id="bath5" autocomplete="off">5+
									</label>
								</div>
							</div>
						</div>

						<div class="form-row">
							<div class="form-group col-md-12">
								<div class="form-row">
									<div class="col-md-12">
										<label for="property_carport">Car Spaces</label>
									</div>
								</div>
								<div class="btn-group btn-group-toggle" data-toggle="buttons">
									<label class="btn btn-outline-primary">
										<input type="radio" value="1" name="property_carport" id="car1" autocomplete="off">1+
									</label>
									<label class="btn btn-outline-primary">
										<input type="radio" value="2" name="property_carport" id="car2" autocomplete="off">2+
									</label>
									<label class="btn btn-outline-primary">
										<input type="radio" value="3" name="property_carport" id="car3" autocomplete="off">3+
									</label>
									<label class="btn btn-outline-primary">
										<input type="radio" value="4" name="property_carport" id="car4" autocomplete="off">4+
									</label>
									<label class="btn btn-outline-primary">
										<input type="radio" value="5" name="property_carport" id="car5" autocomplete="off">5+
									</label>
								</div>
							</div>
						</div>
						<?php if ($post_type == 'rental') : ?>
							<div class="form-check">
								<input type="checkbox" class="form-check-input" id="petFriendly" name="property_pet_friendly" value="yes">
								<label class="form-check-label" for="petFriendly">Pet Friendly</label>
							</div>
						<?php endif; ?>

					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-outline-primary btn-sm" data-dismiss="modal">Save Filters</button>
						<button id="btnFetch" name="submit" type="submit" class="btn btn-primary btn-sm"><i class="fa fa-search" aria-hidden="true"></i> Search</button>
					</div>
				</div>
			</div>
		</div>

	</form>
</div>

<style>
	#address_search div.bootstrap-select {
		width: 100% !important;
	}

	#address_search div.bootstrap-select button {
		max-height: 38px;
	}

	#address_search div.bootstrap-select button:hover,
	#address_search div.bootstrap-select button:focus,
	#address_search div.bootstrap-select button:active {
		color: #fff;
	}

	.dropdown-item.active,
	.dropdown-item:active {
		color: #fff !important;
		text-decoration: none;
		background-color: #ff753847;
	}
</style>
<script>
	jQuery(document).ready(function() {
		jQuery("#btnFetch").click(function() {
			jQuery(this).html('<span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span> Searching...');
		});
		jQuery("#PrimarySubmit").click(function() {
			jQuery(this).html('<span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span> Searching...');
		});
	});
</script>