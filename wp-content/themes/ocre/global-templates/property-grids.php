<?php


$open_home_args = [
	'post_type'         => 'property',
	'post_status'		=> 'publish',
	'order'             => 'DESC',
	'orderby'           => 'date',
	'posts_per_page'    => 3,
	'update_post_term_cache' => false,
	'update_post_meta_cache' => false,
	'no_found_rows'         => false,
	'ignore_sticky_posts'	=> true,
	'meta_query' => [
		[
			'key'     => 'property_inspection_times',
			'compare' => '!=',
			'value' => '',
		]]
];

$current_args = [
	'post_type'         => 'property',
	'post_status'		=> 'publish',
	'order'             => 'DESC',
	'orderby'           => 'date',
	'paged'				=> false,
	'posts_per_page'    => 3,
	'update_post_term_cache' => false,
	'update_post_meta_cache' => false,
	'no_found_rows'         => false,
	'ignore_sticky_posts'	=> true,
	'meta_query' => [
		[
			'key'     => 'property_status',
			'value'   => 'current',
			'compare' => 'LIKE',
		]
	]
];

$args = [
	'post_type'         => 'rental',
	'post_status'		=> 'publish',
	'order'             => 'DESC',
	'orderby'           => 'date',
	'update_post_term_cache' => false,
	'update_post_meta_cache' => false,
	'no_found_rows'         => false,
	'ignore_sticky_posts'	=> true,
	'posts_per_page'    => 3,
	'meta_query' => [
		[
			'key'     => 'property_status',
			'value'   => 'current',
			'compare' => 'LIKE',
		]]
];

$sold_args = [
	'post_type'         => 'property',
	'post_status'		=> 'publish',
	'order'             => 'DESC',
	'orderby'           => 'date',
	'no_found_rows'		=> 'false',
	'update_post_meta_cache' => false,
	'update_post_term_cache' => false,
	'no_found_rows'         => false,
	'ignore_sticky_posts'	=> true,
	'posts_per_page'    => 3,
	'meta_query' => [
		[
			'key'     => 'property_status',
			'value'   => 'sold',
			'compare' => 'LIKE',
		]]
];

$offmarket_args = [
	'post_type'         => 'property',
	'post_status'		=> 'publish',
	'order'             => 'DESC',
	'orderby'           => 'date',
	'update_post_term_cache' => false,
	'update_post_meta_cache' => false,
	'no_found_rows'         => false,
	'ignore_sticky_posts'	=> true,
	'posts_per_page'    => 3,
	'meta_query' => [
		'relation'=>'AND',
		[
			'key'     => 'property_com_exclusivity',
			'value'   => '1',
			'compare' => 'LIKE',
		],
		[
			'key'=>'property_status',
			'value'=>'current'
		]
	]
];

?>

<div class="container mt-120">
	<div class="row">
		<div class="col">
			<ul class="nav nav-pills" id="myTab" role="tablist">
				<li class="nav-item" role="presentation">
					<a href="/sales-open-homes/" class="nav-link" id="open-homes-tab"  href="#open-homes" role="tab" aria-controls="open-homes" aria-selected="true">Open Homes</a>
				</li>
				<li class="nav-item" role="presentation">
					<a class="nav-link active" id="for-sale-tab"  href="#for-sale" role="tab" aria-controls="for-sale" aria-selected="true">For Sale</a>
				</li>
				<li class="nav-item" role="presentation">
					<a href="https://www.ocprojects.com.au/projects-blogs/project/" class="nav-link" id="projects-tab"  href="#projects" role="tab" aria-controls="projects" target="_blank" aria-selected="false">Off the plan</a>
				</li>
				<li class="nav-item" role="presentation">
					<a href="/homes-for-rent" class="nav-link" id="for-rent-tab"  href="#for-rent" role="tab" aria-controls="for-rent" aria-selected="false">For Rent</a>
				</li>
				<li class="nav-item" role="presentation">
					<a href="/property/?action=epl_search&post_type=property&property_status=sold" class="nav-link" id="sold-tab"  href="#sold" role="tab" aria-controls="sold" aria-selected="false">Sold</a>
				</li>
				<li class="nav-item" role="presentation">
					<a href="/sales-oc-exclusive/" class="nav-link" id="off-market-tab" href="#off-market" role="tab" aria-controls="off-market" aria-selected="false">OC First</a>
				</li>
			</ul>
			<div class="tab-content" id="myTabContent">
				<div class="tab-pane fade" id="open-homes" role="tabpanel" aria-labelledby="open-homes">
					<div class="container">
							<div class="row">
								<div class="col-md-6">
									<h2 class="mb-0">Open Homes</h2>
									<p class="subheader mb-4">Properties with upcoming inspection times</p>
								</div>
								<div class="col-md-6 desktop-only">
									<div class="btn-group float-right" role="group">
										<a href="#buyer-alerts" data-toggle="modal" class="btn btn-sm btn-link">Buyer Alerts</a>
										<a href="/sales-open-homes/" class="btn btn-outline-primary btn-sm float-right">View all open home times</a>
									</div>
								</div>
							</div>
							<div class="row">
								<?php //oc_property_grid($open_home_args); ?>
								<div class="col-md-12 mobile-only">
									<div class="btn-group float-right" role="group">
										<a href="/sales-open-homes/" class="btn btn-outline-primary btn-sm float-right">View all open home times</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				<div class="tab-pane fade show active" id="for-sale" role="tabpanel" aria-labelledby="for-sale-tab">
					<div class="container">
					<div class="row">
						<div class="col-md-6">
							<h2 class="mb-0">Homes for Sale</h2>
							<p class="subheader mb-4">Properties available by Ouwens Casserly</p>
						</div>
						<div class="col-md-6 desktop-only">
							<div class="btn-group float-right" role="group">
								<a href="#buyer-alerts" data-toggle="modal" class="btn btn-sm btn-link">Buyer Alerts</a>
								<a href="/property/?action=epl_search&post_type=property&property_status=current" class="btn btn-outline-primary btn-sm float-right">View all properties for sale</a>
							</div>
						</div>
					</div>
						<div class="row">
							<?php oc_property_grid($current_args); ?>
							<div class="col-md-12 mobile-only">
								<div class="btn-group float-right" role="group">
									<a href="/property/?action=epl_search&post_type=property&property_status=current" class="btn btn-outline-primary btn-sm float-right">View all properties for sale</a>
								</div>
							</div>
						</div>
					</div>
				</div>	
				<div class="tab-pane fade" id="for-rent" role="tabpanel" aria-labelledby="for-rent-tab">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<h2 class="mb-0">Rent</h2>
							<p class="subheader mb-4">Properties for lease</p>
						</div>
						<div class="col-md-6 desktop-only">
							<div class="btn-group float-right" role="group">
								<a href="/homes-for-rent" class="btn btn-outline-primary btn-sm float-right">View all rentals</a>
							</div>
						</div>
					</div>
						<div class="row">
							<?php //oc_property_grid($args); ?>
							<div class="col-md-12 mobile-only">
								<div class="btn-group float-right" role="group">
									<a href="/homes-for-rent" class="btn btn-outline-primary btn-sm float-right">View all rentals</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane fade" id="sold" role="tabpanel" aria-labelledby="sold-tab">
					<div class="container">
					<div class="row">
						<div class="col-md-6">
							<h2 class="mb-0">Sold</h2>
							<p class="subheader mb-4">Recently sold homes</p>
						</div>
						<div class="col-md-6 desktop-only">
							<div class="btn-group float-right" role="group">
								<a href="#buyer-alerts" data-toggle="modal" class="btn btn-sm btn-link">Buyer Alerts</a>
								<a href="/property/?action=epl_search&post_type=property&property_status=sold" class="btn btn-outline-primary btn-sm float-right">View all sold properties</a>
							</div>
						</div>
					</div>
						<div class="row">
							<?php //oc_property_grid($sold_args); ?>
							<div class="col-md-4 mobile-only">
								<div class="btn-group float-right" role="group">
									<a href="/property/?action=epl_search&post_type=property&property_status=sold" class="btn btn-outline-primary btn-sm float-right">View all sold properties</a>
								</div>
						</div>
						</div>
					</div>
		
				</div>
				<div class="tab-pane fade" id="off-market" role="tabpanel" aria-labelledby="off-market-tab">
					<div class="container">
					<div class="row">
						<div class="col">
							<h2 class="mb-0">OC Off Market</h2>
							<p class="subheader mb-4">Exclusive properties to Ouwens Casserly</p>
						</div>
						<div class="col-md-6 desktop-only">
							<div class="btn-group float-right" role="group">
								<a href="#buyer-alerts" data-toggle="modal" class="btn btn-sm btn-link">Buyer Alerts</a>
								<a href="/sales-oc-exclusive/" class="btn btn-outline-primary btn-sm">View all off market properties</a>
							</div>
						</div>
					</div>							
						<div class="row">
							<?php //oc_property_grid($offmarket_args); ?>
							<div class="col-md-4 mobile-only">
							<div class="btn-group float-right" role="group">
								<a href="/sales-oc-exclusive/" class="btn btn-outline-primary btn-sm float-right">View all off market properties</a>
							</div>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>