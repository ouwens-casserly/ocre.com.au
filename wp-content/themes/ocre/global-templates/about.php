<div id="about-block" class="wrapper text-center bg-dark text-white pt-5 pb-5 mt-120">
    <div class="container mt-3 mb-3">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <p class="display-1"><span>Ouwens Casserly</span> Real Estate</p>
					<h1 class="subheading-1">Real Estate Agents &amp; Property Management Services in Adelaide</h1>
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <p class="subheading-2">Ouwens Casserly is a dynamic & innovative property firm based in South Australia, selling more than 1000 homes a year and managing close to 1700 investment properties.</p>
                        <a href="/about-us/meet-the-team/" class="btn-outline-light btn btn-sm">Meet our team</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>