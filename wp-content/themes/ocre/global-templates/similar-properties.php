<?php

global $property;

$post_type = get_post_type();

if ( $post_type  == 'property' ) {

	$lower = $property->meta['property_price'][0] * 0.8;
	$upper = $property->meta['property_price'][0] * 1.2;
	$property_key = 'property_price';
	$post_type = 'property';

} elseif ( $post_type  == 'rental' ) {

	$lower = $property->meta['property_rent'][0] * 0.8;
	$upper = $property->meta['property_rent'][0] * 1.2;
	$property_key = 'property_rent';
	$post_type = 'rental';
}

$similar_property_args = [
	'post_type'         => $post_type,
	'order'             => 'DESC',
	'orderby'           => 'date',
	'posts_per_page'    => 3,
	'post__not_in' => array($property->post->ID),
	'meta_query' => [
		[
			'key'     => 'property_status',
			'value'   => 'current',
			'compare' => 'LIKE',
		],
		[
			'key' => $property_key,
			'value' => $lower,
			'type' => 'numeric',
			'compare' => '>='
		],
		[
			'key' => $property_key,
			'value' => $upper,
			'type' => 'numeric',
			'compare' => '<='
		]
	]
];

$general = [
	'post_type'         => $post_type,
	'order'             => 'DESC',
	'orderby'           => 'date',
	'posts_per_page'    => 3,
	'post__not_in' => array($property->post->ID),
	'meta_query' => [
		[
			'key'     => 'property_status',
			'value'   => 'current',
			'compare' => 'LIKE',
		]
	]
];

$similar_query = new WP_Query($similar_property_args);

if ( $similar_query->post_count == 0 ) {
	$title_related = 'More by OC';
	$subtitle_related = 'Other properties available by Ouwens Casserly';
	$similar = $general;
} else {
	$title_related = 'Similar Properties';
	$subtitle_related = 'Properties within a similar price range to <strong>'.$property->meta['property_address_street'][0];
	$similar = $similar_property_args;
}

?>

<div class="inner-wrapper pt-90 pb-90 mt-5">
	<div class="container">
		<div class="row section-header">
			<div class="col-md-8">
				<h2 class="mb-0"><?php echo $title_related; ?></h2>
				<p class="subheader mb-4"><?php echo $subtitle_related; ?></strong></p>
			</div>
			<div class="col-md-4">
				<a href="/property/?action=epl_search&post_type=<?php echo $post_type; ?>&property_status=current" class="btn btn-outline-primary btn-sm float-right sm-100">View all properties</a>
			</div>
		</div>

		<div class="row">
			<?php oc_property_grid($similar); ?>
		</div>
	</div>
</div>

<?php wp_reset_postdata(); ?>