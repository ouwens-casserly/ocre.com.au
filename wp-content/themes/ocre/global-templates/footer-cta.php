<div id="footer-cta" class="wrapper">
    <div class="container" style="z-index: 2;">
        <div class="row">
            <div class="col-md-6">
                <p class="display-1"> Here at <span>Ouwens Casserly Real Estate</span> we are, by nature, an optimistic bunch.</p>
                <h2>Driven, passionate and focused on helping people realise their dreams through property.</h2>
                <p class="p-1">And yet, to see how far we have come since our launch in 2014 is extraordinary. 
                    Our goal was always to create a business focused on its customers and their needs. 
                    A standout brand that people could confidently turn to during one of the most exciting, 
                    overwhelming and daunting moments in their lives.
                </p>
                <div role="toolbar" aria-label="Toolbar with button groups">
                    <div class="btn-group mr-2" role="group" aria-label="First group">
                        <a href="/about-us/meet-the-team/" type="button" class="btn btn-primary btn-sm">Meet our team</a>

                    </div>
                    <div class="btn-group mr-2" role="group" aria-label="Second group">
                        <a href="/our-story/" type="button" class="btn btn-outline-primary btn-sm">Learn about us</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 offset-md-2 polaroid-images">
                <img src="<?php echo get_template_directory_uri();?>/media/banner.jpg" class="polaroid">
                <img src="<?php echo get_template_directory_uri();?>/media/dog.jpg" class="polaroid">
            </div>
        </div>
    </div>
</div>

<div id="footer-cta-2" class="wrapper pt-120 pb-120">
    <div class="container">
        <div class="row">
            <div class="col-md-6 offset-md-3">    
            <p class="display-1 text-center"> We are here to <span>help.</span></p>
            <p class="p-1 text-center">
            Our purpose is to help people realise their dreams and aspirations through property. 
            Whether you are selling, buying, investing, renting or developing we are here to help achieve your property goals.
            </p>
            <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups text-center">
                    <div class="btn-group mr-2" role="group" aria-label="First group">
                        <a href="/properties-for-sale/" type="button" class="btn btn-outline-primary btn-sm">Properties for sale</a>

                    </div>
                    <div class="btn-group mr-2" role="group" aria-label="Second group">
                        <a href="/sell/" type="button" class="btn btn-link btn-sm">Sell with OC</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>