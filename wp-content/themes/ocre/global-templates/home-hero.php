<div class="jumbotron jumbotron-fluid">

<video id="video-background" preload muted autoplay loop playsinline>
    <source src="/wp-content/themes/ocre/media/ocrehero.mp4" type="video/mp4">
  </video>
  <div id="daintegration"></div>

<script defer src="https://dpr.leadplus.com.au/main.js"></script>

</div>