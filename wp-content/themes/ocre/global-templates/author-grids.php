<?php

$paged_current = isset( $_GET['current'] ) ? (int) $_GET['current'] : 1;
$paged_sold = isset( $_GET['sold'] ) ? (int) $_GET['sold'] : 1;

$user_meta 	= get_user_meta($author);
$agent 		= get_fields('user_'. $author);
$user 		= get_user_by('slug',$user_meta['nickname'][0]);
$post_type = 'property';
$team = get_user_meta($author, 'team_type', true );

if ( in_array('property_management', $team ) ) : 
	$property_type = 'Properties for lease';
	$soldLeased = 'leased';
	$post_type = 'rental';
else : 
	$property_type = 'Properties for sale';
	$soldLeased = 'sold';
endif; 

$author_current_listings = array(
    'post_type'   => $post_type,
    'post_status' => 'publish',
    'posts_per_page' => 15,
	'paged' => $paged_current,
    'meta_query' => array(
		'relation' => 'AND',
        array(
            'key' => 'property_status',
            'value' => 'current',
            'compare' => '=',
		),
		array(
			'relation' => 'OR',
			array(
				'key' => 'property_agent',
				'value' => $user->data->user_login,
				'compare' => '=',
			),
			array(
				'key' => 'property_second_agent',
				'value' => $user->data->user_login,
				'compare' => '=',
			),
		)
    )
);

$author_sold_listings = array(
    'post_type'   => $post_type,
    'post_status' => 'publish',
    'posts_per_page' => 15,
	'paged' => $paged_sold,
	'order' => 'DESC',
    'orderby' => 'meta_value',
    'meta_key' => 'property_sold_date',
    'meta_query' => array(
		'relation' => 'AND',
        array(
            'key' => 'property_status',
            'value' => $soldLeased,
            'compare' => '=',
		),
		array(
			'relation' => 'OR',
			array(
				'key' => 'property_agent',
				'value' => $user->data->user_login,
				'compare' => '=',
			),
			array(
				'key' => 'property_second_agent',
				'value' => $user->data->user_login,
				'compare' => '=',
			),
		),
	)
);

?>

<div id="properties" class="container mt-120 pb-120">
	<div class="row">
		<div class="col">
			<ul class="nav nav-pills" id="myTab" role="tablist">
				<li class="nav-item" role="presentation">
					<a class="nav-link active" id="for-sale-tab" data-toggle="pill" href="#for-sale" role="tab" aria-controls="for-sale" aria-selected="true"><?php echo $property_type; ?></a>
				</li>
				<li class="nav-item" role="presentation">
					<a class="nav-link" id="sold-tab" data-toggle="pill" href="#sold" role="tab" aria-controls="sold" aria-selected="false">Recently <?php echo $soldLeased; ?> by <?php echo $user_meta['first_name'][0]; ?></a>
				</li>
				<li class="nav-item" role="presentation">
					<a class="nav-link" id="reviews-tab" data-toggle="pill" href="#reviews" role="tab" aria-controls="reviews" aria-selected="false">Testimonials</a>
				</li>
			</ul>
			
			<div class="tab-content" id="myTabContent">
				<div class="tab-pane fade show active" id="for-sale" role="tabpanel" aria-labelledby="for-sale-tab">
					<div class="container">
						<div id="current-listings" class="row">

							<?php 

							$current = new WP_Query( $author_current_listings );

							while ( $current->have_posts() ) : $current->the_post();
								echo get_single_paginated_property( get_the_id() );
							endwhile;


							$current_args = array(
								'format'  => '?current=%#%#for-sale-tab',
								'current' => $paged_current,
								'total'   => $current->max_num_pages,
								'add_args' => array( 'sold' => $paged_sold )
							);
							?>

						</div>

					<div class="row">
						<div class="col">
							<div class="pagination">
								<?php echo paginate_links( $current_args ); ?>
							</div>
						</div>
					</div>

					</div>
				</div>	
				<div class="tab-pane fade" id="sold" role="tabpanel" aria-labelledby="sold-tab">
					<div class="container">
						<div class="row">
							<?php 
							
							$sold = new WP_Query( $author_sold_listings );

							while ( $sold->have_posts() ) : $sold->the_post();

								echo get_single_paginated_property( get_the_id() );

							endwhile;

							$pag_args2 = array(
								'format'  => '?sold=%#%#sold',
								'current' => $paged_sold,
								'total'   => $sold->max_num_pages,
								'add_args' => array( 'paged_current' => $paged_current )
							);
		
							?>

						</div>

						<div class="row">
						<div class="col">
							<div class="pagination">
								<?php echo paginate_links( $pag_args2 ); ?>
							</div>
						</div>
					</div>


					</div>

				</div>

				<div class="tab-pane fade" id="reviews" role="tabpanel" aria-labelledby="reviews-tab">
					
				<?php if ( empty($user_meta['agent_rma_id'][0]) ) :
									
					$reviews = array(
						'author'        => $author,
						'post_type'   => 'review',
						'post_status' => 'publish',
						'posts_per_page' => 15,
					);
	
					$reviews_query = new WP_Query($reviews);
	
					if ( $reviews_query->have_posts() ) : ?>
	
					<div id="post-review-slide" class="splide container">

						<div class="splide__track">

							<ul class="splide__list">
	
							<?php while ( $reviews_query->have_posts() ) : $reviews_query->the_post(); ?>
	
								<li class="splide__slide">

									<div class="p-2">

										<div class="polaroid-property p-4">
	
											<?php the_content(); ?>
											<h4><?php the_title(); ?></h4>
											<div class="stars">
												<img class="star" src="/wp-content/plugins/rate-my-agent-ajax-reviews/public/images/star.png">
												<img class="star" src="/wp-content/plugins/rate-my-agent-ajax-reviews/public/images/star.png">
												<img class="star" src="/wp-content/plugins/rate-my-agent-ajax-reviews/public/images/star.png">
												<img class="star" src="/wp-content/plugins/rate-my-agent-ajax-reviews/public/images/star.png">
												<img class="star" src="/wp-content/plugins/rate-my-agent-ajax-reviews/public/images/star.png">
											</div>

										</div>

									</div>
	
								</li>
	
							<?php endwhile; ?>

							</ul>

						</div>
	
					</div>

					<script>
	
					jQuery(function() {

						var reviews = new Splide( '#post-review-slide', {
							perPage: 3,
							rewind : true,
							breakpoints : {
								768: {
									perPage: 1,
								},
							}
						} ).mount();

						$( '[href=#reviews]' ).on( 'shown.bs.tab', function() {  reviews.emit( 'resize' ); });

					});
					</script>
	
					<?php else :

						if( !empty( $agent['reviews_link'] ) ) {
							echo 'Check out my reviews over at <a href="'.$agent['reviews_link'].'" target="_blank">'.$agent['reviews_link'].'</a>';
						} else {
							echo 'I don\'t have any reviews yet.';
						}
	
					endif;
	
					// Restore original Post Data
					wp_reset_postdata();
						
					else :
	
						echo do_shortcode('[rmaa_slider reviews_for="agent" id="'.$user_meta['agent_rma_id'][0].'" sale_type="sales"]');
	
					endif; ?>

                </div>

			</div>

		</div>

	</div>

</div>
