<?php
/**
 * Get the latest posts and display them.
 */

$latest_news = [
	'post_type'         => 'post',
	'order'             => 'DESC',
	'orderby'           => 'date',
	'posts_per_page'    => 3
];

$query = new WP_Query( $latest_news );

?>

<?php if ( $query->have_posts() ) : ?>

	<div id="latest-news-heading" class="container mt-90">

		<div class="row">

			<div class="col-md-8">

				<h2>Latest News</h2>

				<p class="subheading subheader">Straight from the OC News Room</p>

			</div>

			<div class="col-md-4 desktop-only">

				<a href="/real-life/" class="btn btn-outline-primary btn-sm float-right mt-2 sm-100">More from our blog</a>

			</div>

		</div>

	</div>

	<div id="latest-news-container" class="container post-grid pb-120 mb-5">

	    <div class="row">

	    <?php while ( $query->have_posts() ) : ?>

	        <div class="col-md-4">

	            <div class="single-post">

	                <?php $query->the_post(); ?>

					<a href="<?php the_permalink(); ?>">

	                	<?php the_post_thumbnail('large', ['class' => 'property-image']); ?>

					</a>

	                <div>

	                <div class="post-content">

					<a href="<?php the_permalink(); ?>"><?php the_title('<h3>', '</h3>'); ?></a>

	                    <div class="entry-meta">

				            <?php understrap_posted_on(); ?>
		
				            <?php $tags_list = get_the_tag_list( '', ' ' );
				            if ( $tags_list ) { printf( '<span class="tags-links">' . '%s' . '</span>', $tags_list ); } ?>

	                    </div>

	                </div>

	    </div>

	            </div>

	        </div>

	    <?php endwhile; ?>

		<div class="col-md-4 mobile-only">

			<a href="/real-life/" class="btn btn-outline-primary btn-sm float-right mt-2 sm-100">More from our blog</a>

		</div>

	    </div>

	</div>

<?php endif; ?>

<?php wp_reset_postdata(); ?>