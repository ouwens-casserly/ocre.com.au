<?php 
/**
 * This template part gathers all of the components for the top header/gallery 
 * of the single property listing. Components required are:
 * 
 *  	-- Featured Image
 * 		-- Property Status
 * 			- Used to create the top left sold/open home banner
 * 		-- property_floorplan
 * 		-- property_floorplan_2
 * 		-- property_video_url
 * 		
 */

global $property;
$yt = 'https://www.youtube.com/embed/';
/**
 * Add a label based on the property status and if 
 * it has inspection times. 
 * @property
 */ 
switch ($property->get_property_meta('property_status') ) {
	case 'sold':
		$banner = '<span class="status-sold">sold</span>';
		break;
	case 'current':
		if( !empty($property->get_property_meta('property_inspection_times')) ) {
			$banner = '<span class="status-sold">Open Home</span>';
		}
		break;
	case 'leased': 
		$banner = '<span class="status-sold">Leased</span>';
		break;
	default: 
	$banner = '';
}


/**
 * Get the property_video_url and if it exsits, strip it to work
 * in a Youtube frame correctly.
 */
$video = $property->get_property_meta('property_video_url');
if( !empty($video) ) {
	if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/\s]{11})%i', $video, $match)) {
		$video_id = $match[1];
		$yt_cover_image = 'https://img.youtube.com/vi/'.$video_id.'/mqdefault.jpg';
	}
}

$property_images = get_posts(array(
    'post_parent' => get_the_ID(),
    'post_type' => 'attachment',
    'post_mime_type' => 'image',
    'orderby' => 'ID',
    'order' => 'ASC',
	'numberposts' => -1
));

$floor_plan = $property->get_property_meta('property_floorplan');
if(!empty($floor_plan)) {
	$floor_plan = preg_replace("/^https:/i", "http:", $floor_plan);
	$property_images[] = (object) [
		'ID' => 'floor_plan', 
		'sizes' => (object) getimagesize( $floor_plan ),
		'URL' => $floor_plan,
	];
}

$floor_plan_2 = $property->get_property_meta('property_floorplan_2');
if(!empty($floor_plan_2)) {
	$floor_plan_2 = preg_replace("/^https:/i", "http:", $floor_plan_2);
	$property_images[] = (object) [
		'ID' => 'floor_plan', 
		'sizes' => (object) getimagesize( $floor_plan_2 ),
		'URL' => $floor_plan_2,
	];
}

?>

<!-- Initiate lightbox and load JS -->
<script type="module">
	import PhotoSwipeLightbox from '/wp-content/themes/ocre/photoswipe/photoswipe-lightbox.esm.js';
	const lightbox = new PhotoSwipeLightbox({
	  	gallerySelector: '#property-gallery',
	  	childSelector: 'a',
	  	pswpModule: '/wp-content/themes/ocre/photoswipe/photoswipe.esm.js',
	  	pswpCSS: '/wp-content/themes/ocre/photoswipe/photoswipe.css'
	});
	lightbox.on('itemData', (e) => {
		const element = e.itemData.element;
	  	if (element  && element.dataset.pswpIsVideo) {
	    	const videoURL = 'https://www.youtube.com/embed/<?php echo $video_id; ?>';
	    	e.itemData = {
		  		html: '<div class="lightbox-wrapper"><div class="container"><div class="row"><div class="col-md-8 offset-md-2"><div class="embed-responsive embed-responsive-16by9"><iframe id="youtubePlayer" src="https://www.youtube.com/embed/<?php echo $video_id; ?>?controls=1&showinfo=1&rel=0" allowfullscreen></iframe></div></div></div></div></div>'
	    	};
	  	}
	});
	lightbox.init();
</script>

<div class="container">

	<div class="row">

		<div class="col-md-12">

			<div class="pswp-gallery pswp-gallery--single-column" id="property-gallery">

			<?php
			
			/**
			 * Iterate over each image and add to gallery.
			 */
			$markup = '<div class="hero-image">'.$banner;
			$i = 0;
			foreach ( $property_images as $single_image ) :

				switch ($single_image->ID) {
					case '0' :
						$img = [ get_template_directory_uri().'/media/placeholder.jpg', '1500', '1125' ];
						$thumbnail = [ get_template_directory_uri().'/media/placeholder.jpg',  ];
						break;
					case 'floor_plan' :
						$img = [
							$single_image->URL, 
							$single_image->sizes->{0}, 
							$single_image->sizes->{1}, 
							$single_image->sizes->{2}
						];
						$thumbnail = [$single_image->URL];
						break;
					default :
						$img = wp_get_attachment_image_src( $single_image->ID, "full" ); 
						$thumbnail = wp_get_attachment_image_src($single_image->ID, "large");
						
				}

				if ( $i > 3 ) { $classes = 'hidden-image'; }
				if ( $i < 4 ) { $img_thmb = '<img src="'.$thumbnail[0].'" alt="" />'; }
			
				if (!empty($video_id) && $i == 2 ) { // -Start Video output (if it exists) -->

					$markup .= '<div id="img-'.$i.'" class="single-video">';
					$markup .=	'<a id="link-video" href="'.$yt.$video_id.'" data-pswp-is-video="true""><img src="'.$yt_cover_image.'"/></a>';
					$markup .= '</div>';
					$markup .= '<div id="img-3" class="single-image '.$classes.'">';
					$markup .= '<a href="'.$img[0].'" data-pswp-width="'.$img[1].'" data-pswp-height="'.$img[2].'" target="_blank">'.$img_thmb.'</a>';
					$markup .= '</div>';

					$i++; $i++; continue;
	
				} // End video output -->

				$markup .= '<div id="img-'.$i.'" class="single-image '.$classes.'">';
				$markup .= '<a href="'.$img[0].'" data-pswp-width="'.$img[1].'" data-pswp-height="'.$img[2].'" target="_blank">'.$img_thmb.'</a>';
				$markup .= '</div>';

				if( $i == 0 ) {
					$markup .= '</div>';

					if(count($property_images) > 1 ) {
						$markup .='<div class="thumbnails">';
					}

				}

				$i++;

			endforeach;

			echo $markup; 
		
			?>

		</div>
			
	</div>

</div>

<style>
#property-gallery .thumbnails #img-3::after { 
	content: 'View <?php echo count($property_images) - 3; ?> more photos';
}
</style>