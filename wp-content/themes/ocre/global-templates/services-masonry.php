<div id="services-masonry" class="container mt-120">
    <div class="row">
        <div class="col-md-3">
            <a href="/properties-for-sale/">
                <div class="masonry-item one">
                    <div class="masonry-content">
                        <h2 class="text-center text-white">Buy</h2>
                        <button class="btn btn-outline-light btn-sm">View properties for sale</button>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-9">
            <div class="row">
                <div class="col-md-6">
                    <a  href="/properties-for-rent/">
                        <div class="masonry-item two">
                            <div class="masonry-content">
                                <h2 class="text-center text-white">Rent</h2>
                                <button class="btn btn-outline-light btn-sm">View properties for rent</button>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-6">
                    <a  href="/sell/">
                        <div class="masonry-item three">
                            <div class="masonry-content">
                                <h2 class="text-center text-white">Sell</h2>
                                <button class="btn btn-outline-light btn-sm">Sell with us</button>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <a href="/manage">
                        <div class="masonry-item four">
                            <div class="masonry-content">
                                <h2 class="text-center text-white">Manage</h2>
                                <button class="btn btn-outline-light btn-sm">Let us manage your property</button>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-4">
                    <a href="https://ocprojects.com.au/" target="_blank">
                        <div class="masonry-item five">
                            <div class="masonry-content">
                                <h2 class="text-center text-white">Projects</h2>
                                <button class="btn btn-outline-light btn-sm">Learn more</button>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>