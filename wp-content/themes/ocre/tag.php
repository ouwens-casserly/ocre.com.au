<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package UnderStrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

$page_for_posts = get_option( 'page_for_posts' ); 

if ($page_for_posts && has_post_thumbnail($page_for_posts) ) {
    $thumb_id = get_post_thumbnail_id( $page_for_posts);
    $url = wp_get_attachment_url( $thumb_id );
} else {
    $url = '';
}

?>

<div class="wrapper" id="index-wrapper">

    <div class="jumbotron jumbotron-fluid" style="background-image: url(<?php echo $url; ?>); background-size: cover;">

        <div id="hero" class="container">

          <div class="row">

            <div class="col-md-12">

				<header class="page-header">
						<?php the_archive_title( '<h1 class="display-1 text-light font-italic">', '</h1>' ); ?>
						<?php the_archive_description(  ); ?>
				</header><!-- .page-header -->

            </div>

        </div>

      </div>

    </div>

    <div class="white-wrapper">

        <div class="container">

            <div id="category-list" class="row">

                <div class="col-md-8">

                    <ul class="nav nav-pills">

                   <?php $categories = get_categories(); 
                   
                   foreach ( $categories as $category ) :  ?>

                        <li class="nav-item"><a class="nav-link" href="/<?php echo get_option( 'category_base' ).'/'.$category->slug;?>"><?php echo $category->name; ?></a></li>

                   <?php endforeach; ?>

                    </ul>                    
                   
                </div>

                <div id="search-bar-container" class="col-md-4">

                    <?php echo get_search_form(); ?>

                </div>

            </div>

        </div>

    </div>

    <div class="container">

<div class="row">

    <div class="col">

    <?php if ( function_exists('yoast_breadcrumb') ) yoast_breadcrumb( '<p id="breadcrumbs">','</p>' ); ?>

    </div>

</div>

</div>

	<div class="container" id="content" tabindex="-1">

		<div class="row">

            <div class="container content-area" id="primary">

			    <main class="site-main row" id="main">

			    	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

			    		<?php get_template_part( 'loop-templates/content', get_post_format() ); ?>

			    	<?php endwhile; endif; ?>

			    </main>

			<?php understrap_pagination(); ?>

			</div>

		</div><!-- .row -->

	</div><!-- #content -->

</div><!-- #index-wrapper -->

<?php //get_template_part( 'global-templates/latest-news' ); ?>	

<?php //get_template_part( 'global-templates/footer-cta' ); ?>	

<?php
get_footer();
