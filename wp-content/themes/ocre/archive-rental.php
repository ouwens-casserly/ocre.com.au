<?php
/**
 * The Default Template for displaying all Easy Property Listings archive/loop posts with WordPress Themes
 *
 * @package EPL
 * @subpackage Templates/Themes/Default
 * @since 1.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

get_header(); ?>

<div id="content" role="main">

	<div class="container">

		<?php if ( have_posts() ) : ?>

		<div class="row">

			<div class="col-md-12">

				<header class="archive-header entry-header loop-header">

					<h1 class="archive-title loop-title">

						<?php do_action( 'epl_the_archive_title' ); ?>

					</h1>

				</header>

			</div>

		</div>

		<div class="entry-content loop-content <?php echo esc_attr( epl_template_class( 'default', 'archive' ) ); ?>">
		
			<?php do_action( 'epl_property_loop_start' ); ?>

			<div class="row post-grid">

				<?php while ( have_posts() ) : the_post(); do_action( 'epl_property_blog' ); endwhile; ?>

				<?php do_action( 'epl_property_loop_end' ); ?>

			</div>

		</div>

		<div class="row">

			<div class="col-md-12">

				<div class="loop-footer">

					<div class="loop-utility clearfix">

						<?php do_action( 'epl_pagination' ); ?>

					</div>

				</div>

			</div>

		</div>

		<?php else : ?>

			<div class="hentry">

				<?php do_action( 'epl_property_search_not_found' ); ?>

			</div>

		<?php endif; ?>

	</div>

</div>

<?php
get_sidebar();
get_footer();
