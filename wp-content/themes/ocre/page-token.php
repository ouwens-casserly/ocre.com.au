<?php

$curl = curl_init();

$token = get_option('rmaar_temp_token');
$token = "Authorization: Bearer " . $token;

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://developers.ratemyagent.com.au/agency/ar943/leasing/reviews/",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_HTTPHEADER => array(
    "Content-Type: application/json",
    $token ),
));

$response = curl_exec($curl);


$body = json_decode($response);
var_dump($body);

curl_close($curl);