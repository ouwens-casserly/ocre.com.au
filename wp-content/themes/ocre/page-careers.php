<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package UnderStrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$current_args = [
	'post_type'         => 'position',
	'order'             => 'DESC',
	'orderby'           => 'date',
	'paged'				=> false,
	'posts_per_page'    => -1,
	'meta_query' => [
		[
			'key'     => 'status',
			'value'   => 'current',
			'compare' => 'LIKE',
		]]
];
$current_positions = new WP_Query( $current_args );

$eoi_args = [
	'post_type'         => 'position',
	'order'             => 'DESC',
	'orderby'           => 'date',
	'paged'				=> false,
	'posts_per_page'    => -1,
	'meta_query' => [
		[
			'key'     => 'status',
			'value'   => 'expression_of_interest',
			'compare' => 'LIKE',
		]]
];
$eoi_positions = new WP_Query( $eoi_args );

?>

<div class="wrapper" id="page-wrapper">
	
    <div class="jumbotron jumbotron-fluid mb-5">

        <video id="video-background" preload muted autoplay loop>
            <source src="/wp-content/themes/ocre/media/careers.mp4" type="video/mp4">
        </video>

        <div id="hero" class="container">

            <div class="row">

                <div class="offset-xl-3 offset-md-3 col-md-offset-0 offset-sm-0 col-md-6">

                    <h2 class="display-1 text-center text-light font-italic"><span>OC</span> Careers</h2>

                    <p class="lead text-center text-light">Discover More below.</p>

                </div>

            </div>

        </div>

    </div>

    <div class="container">
		
        <div class="row">

	    	<div class="col-md content-area" id="primary">

	    		<main class="site-main" id="main">

	    			<?php while ( have_posts() ) : ?>

                        <div class="row">

                            <div class="col-md-12">

                                <?php the_post(); the_content(); ?>

                                <div class="row mb-90 mt-90">

                                    <div class="col-md-6">

                                    <h2>Our Story</h2>
                                    <p>Ouwens Casserly also known as OC, is one of South Australia leading independent residential property firms.<p>
                                    <p>OC is ultimately about making it happen for the consumer. We are a client and service obsessed brand designed for momentum. Our brand is clean, authentic, and straight-talking. Professional and down to earth but still premium. We continually aspire to be a business that enables people to achieve their goals and aspirations through property.</p>

                                    <p>Why join OC? We believe that working hard and having fun along the way is the key to success and the reason we not only attract but retain knowledgeable, passionate, genuine people.</p>
                                    <h2 class="display-2 italic">Make It Happen™</h2>
                                    </div>

                                    <div class="col-md-6 mt-5">

                                        <div class="row post-grid">

                                            <div class="col-md-6 mb-4">
                                                <h4>Energetic & <span>Vibrant</span> Culture</h4>
                                                <p class="label">A growing & talented team who have fun along the way!</p>
                                            </div>

                                            <div class="col-md-6 mb-4">
                                                <h4>Training & <span>Development</span></h4>
                                                <p class="label">Our new online Learning Management System is designed specifically to provide our team members with the tools & training they need to be successful.</p>
                                            </div>

                                            <div class="col-md-6 mb-4">
                                                <h4><span>Reward</span> & Recognition</h4>
                                                <p class="label">Professional & personal development opportunities with access to leadership & mentoring programs.</p>
                                            </div>

                                            <div class="col-md-6 mb-4">
                                                <h4>Career <span>Progression</span></h4>
                                                <p class="label">We understand & value the importance of a healthy work life balance.</p>
                                            </div>

                                            <div class="col-md-6 mb-4">
                                                <h4><span>Flexible</span> & Remote</h4>
                                                <p class="label">To recognise & celebrate the successes & hard work of our team.</p>
                                            </div>

                                            <div class="col-md-6 mb-4">
                                                <h4><span>Wellness</span> Program</h4>
                                                <p class="label">Your well being matters & you will get access to our wellness program.</p>
                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>
                    

                        </div>

                    
                    <?php endwhile; ?>

	    		</main><!-- #main -->

	    	</div>

	    </div><!-- .row -->

    </div>

    <div class="wrapper text-center bg-dark text-white pt-5 pb-5 mb-90">
	
        <div class="container mt-3 mb-3">
		
            <div class="row">
			
                <div class="col-md-12">

                    <div class="row">

                        <div class="col-md-12">
                        
                            <h2 class="col-12 text-center text-light display-2" style="text-align: center;">Our values</h2>
                            <p class="col-12 text-center text-light mb-5" style="text-align: center;">Our values are the foundation of the culture we have created.  It is the way we approach business internally, as well as externally.</p>

                        </div>
    
                        <div class="col-md-3 text-center pr-3 feature-container mb-5">

                            <h4><span>Authenticity</span></h4>
                            <p class="paragraph-2">In every minute of everyday. Genuine, ethical, trusted, accountable.</p>
    
                        </div>

                        <div class="col-md-3 text-center pr-3 feature-container mb-5">
                            <h4><span>Optimism</span></h4>
                            <p class="paragraph-2">In our attitude. Positive, upbeat and energetic.</p>
                        </div>

                        <div class="col-md-3 text-center pr-3 feature-container mb-5">
                            <h4><span>Passion</span></h4>
                            <p class="paragraph-2">About working hard and having fun. We love what we do and we love helping people.</p>
                        </div>
                        
                        <div class="col-md-3 text-center pr-3 feature-container mb-5">
                            <h4><span>Precision</span></h4>
                            <p class="paragraph-2">Our operation is innovative and world class. Our process, training and standards ensure exceptional service and results.</p>
                        </div>
                
                    </div>
				
                </div>

			</div>
				
        </div>

    </div>

    <div class="container">

        <div class="row">
            
            <div class="col-md-12">
                
                <div class="videoWrapper">
            
                    <iframe src="https://www.youtube.com/embed/cKviOm8iZ0k" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                
                </div>
            
            </div>
            
        </div>

        <div class="row mt-5">

            <div class="col-md-5">
                <img src="/wp-content/themes/ocre/images/careers.jpg">
            </div>

            <div class="col-md-5 offset-md-1">
                <h3 class="mb-5">What Our <span>Team</span> Say</h3>
                <h4><span>100%</span><h4>
                    <p class="mb-5">of our people are proud to work for Ouwens Casserly</p>
                    <h4><span>82%</span><h4>
                    <p class="mb-5">Overall engagement score (benchmark for AUS is 70%)</p>
                    <h4><span>92%</span><h4>
                    <p class="mb-5">Of employees at OC believe we adapted well to changes in working conditions throughout Covid-19</p>
                    <h4><span>91%</span><h4>
                    <p class="mb-5">Of our employees said they had complete confidence in our response to the Covid-19 Pandemic</p>

            </div>
        
        </div>

        <div class="row mt-5">
            <div class="col-md-12">
                <h2>Current Career Opportunities</h2>
            </div>
        </div>
        
        <div class="row search">
        
        <?php if ( $current_positions->have_posts() ) : ?>

            <?php while ( $current_positions->have_posts() ) : ?>

                <div class="col-md-4">

                    <article>

                        <?php $current_positions->the_post(); ?>

                        <h3><?php echo get_the_title(); ?></h3>
                        <?php echo the_excerpt(); ?>
                        <a href="<?php echo the_permalink(); ?>" class="btn btn-outline-primary btn-sm">DISCOVER MORE</a>

                    </article>

                </div>
            
            <?php endwhile; ?>
        
        <?php endif; ?>

        <?php wp_reset_postdata(); ?>

        </div>

        <div class="row mt-5">
            <div class="col-md-12">
            <h2>Expressions Of Interest</h2>
            <p>Didn't see your dream role listed above? We encourage you to register for future opportunities so you can be a part of our talent bank.</p>
        </div>
        </div>

        <div class="row search">
        
        <?php if ( $eoi_positions->have_posts() ) : //eoi_positions ?>

            <?php while ( $eoi_positions->have_posts() ) : ?>

                <div class="col-md-4">

                    <article>

                        <?php $eoi_positions->the_post(); ?>

                        <h3><?php echo get_the_title(); ?></h3>
                        <?php echo the_excerpt(); ?>
                        <a href="<?php echo the_permalink(); ?>" class="btn btn-outline-primary btn-sm">DISCOVER MORE</a>

                    </article>

                </div>
            
            <?php endwhile; ?>
        
        <?php endif; ?>

        <?php wp_reset_postdata(); ?>

        </div>

    </div>

    <?php get_template_part( 'global-templates/latest-news' ); ?>	

    <?php get_template_part( 'global-templates/about' ); ?>

</div><!-- #page-wrapper -->

<?php
get_footer();
