<?php
/**
 * Template Name: Landing Page V1
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package UnderStrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

$background_colour = get_field( 'background_colour' );

?>

<div class="wrapper" id="page-wrapper" style="background-color: <?php echo $background_colour; ?>;">

<?php 
				
	if( have_rows('landing_page_builder') ) :

		while( have_rows('landing_page_builder') ) : the_row();

			if( get_row_layout() == 'full_width_image' ) :

			    $banner_image = get_sub_field('banner_image'); ?>

				<div class="container-fluid full-width-image pb-90">

					<div class="row">
						
						<?php echo wp_get_attachment_image($banner_image, 'full'); ?>

					</div>

				</div>

			<?php endif; // End full_width_image

			if( get_row_layout() == 'full_width_banner' ) :
			
				$banner_image = get_sub_field('wysiwyg_banner'); ?>

				<div class="wrapper text-center bg-dark text-white pt-5 pb-5 mb-90">
				    <div class="container mt-3 mb-3">
						<div class="row">
							<div class="col-md-8 offset-md-2">
								<?php 
								$wysiwyg_banner = get_sub_field('wysiwyg_banner');
								echo $wysiwyg_banner; 
								?>
							</div>
				    	</div>
					</div>
				</div>
			
			<?php endif; // End full_width_image

			if( get_row_layout() == 'full_width_image_with_white_text' ) :

				$banner_image = get_sub_field('image');
				$banner_image = wp_get_attachment_image_url($banner_image, 'full'); ?>

				<div class="full_width_image_with_white_text wrapper text-center bg-dark text-white pt-5 pb-5 mb-90" style="background-image: url(<?php echo $banner_image; ?>);">
					<div class="container mt-3 mb-3 pt-5 pb-5" style="z-index:2; position: relative;">
						<div class="row pt-5 pb-5">
							<div class="col-md-8 offset-md-2">
								<?php 
								$wysiwyg_banner = get_sub_field('wysiwyg_editor');
								echo $wysiwyg_banner; 
								?>
							</div>
						</div>
					</div>
				</div>
			
			<?php endif; // End full_width_image_with_white_text

			if( get_row_layout() == 'full_width_wysiwyg_editor' ) :

	            $textarea = get_sub_field('textarea'); 
				$container_class = get_sub_field('container_class'); ?>

				<div class="container pb-90">

					<div class="row align-items-center">

						<div class="<?php echo $container_class; ?>">
						
							<?php echo $textarea; ?>

						</div>

					</div>

				</div>

	        <?php endif; // End full_width_wysiwyg_editor

			if( get_row_layout() == 'property_grid' ) :
			
				$post_type = get_sub_field('sales_or_rentals');
				$post_type == 'sales' ? $post_type = 'property' : $post_type = 'rental';
				$property_status = get_sub_field('current');
				$suburb = get_sub_field('suburb');
				$agents = get_sub_field('sales_agent');
				$title = get_sub_field('section_title');
				$subtitle = get_sub_field('section_subtitle');
				$button_text = get_sub_field('property_button_text');
				$button_url = get_sub_field('property_button_url');

				$current_args = [
					'author'			=> $agents,
					'post_type'         => $post_type,
					'order'             => 'DESC',
					'orderby'           => 'date',
					'paged'				=> false,
					'posts_per_page'    => 3,
					'meta_query' => [
						[
							'key'     => 'property_status',
							'value'   => $property_status,
							'compare' => 'LIKE',
						]
					]
				];

				if ( !empty( $suburb ) ) {
					$current_args['tax_query'] = [[
						'taxonomy' => 'location',
						'field' => 'slug',
						'terms' => $suburb->slug,
					]];
				}

				?>
				
				<div class="grid-wrapper pt-5 pb-5" style="background-color: #f8faf5;">
					<div class="container pb-5 pt-5">
						<div class="row">
							<div class="col-md-6">
								<h2 class="mb-0"><?php echo $title; ?></h2>
								<p class="subheader mb-4"><?php echo $subtitle; ?></p>
							</div>
							<div class="col-md-6 desktop-only">
								<div class="btn-group float-right" role="group">
									<a href="#buyer-alerts" data-toggle="modal" class="btn btn-sm btn-link">Buyer Alerts</a>
									<a href="<?php echo $button_url; ?>" class="btn btn-outline-primary btn-sm float-right"><?php echo $button_text; ?></a>
								</div>
							</div>
						</div>
							<div class="row">
								<?php oc_property_grid($current_args); ?>
								<div class="col-md-12 mobile-only">
									<div class="btn-group float-right" role="group">
										<a href="<?php echo $button_url; ?>" class="btn btn-outline-primary btn-sm float-right"><?php echo $button_text; ?></a>
									</div>
								</div>
							</div>
						</div>
				</div>

				<?php endif; // End property_grid




			if( get_row_layout() == 'left_wysiwyg_with_right_image' ) :

	            $wysiwyg_editor = get_sub_field('wysiwyg_editor'); ?>

				<div class="container pb-90">

					<div class="row align-items-center">

						<div class="col-md-6 entry-content">
	
							<?php echo $wysiwyg_editor; ?>

						</div>

						<div class="col-md-1"></div>

				<?php $image_right = get_sub_field('image_right'); ?>

						<div class="col-md-1"></div>

						<div class="col-md-4 polaroid-container">

							<?php echo wp_get_attachment_image($image_right, 'full'); ?>

						</div>

					</div>

				</div>
							
			<?php endif; // End left_wysiwyg_with_right_image


			if( get_row_layout() == 'right_wysiwyg_with_left_image' ) : ?>
		
				<div class="container pb-90">
			
					<div class="row align-items-center">
			
						<div class="col-md-4 polaroid-container mb-90">

							<?php $image_left = get_sub_field('image_left'); ?>

							<?php echo wp_get_attachment_image($image_left, 'full'); ?>

						</div>

						<div class="col-md-1"></div>
			
						<div class="col-md-7 entry-content">

							<?php $wysiwyg_editor = get_sub_field('wysiwyg_editor'); ?>
			
							<?php echo $wysiwyg_editor; ?>
			
						</div>
			
					</div>
			
				</div>

			<?php endif; // End right_wysiwyg_with_left_image




			if( get_row_layout() == 'testimonials' ) :

				$show_testimonials 	= get_sub_field('show_testimonials');
				$testimonial_type 	= get_sub_field('testimonial_type');
				$rma_id 			= get_sub_field('rma_id');
				$reviews_for 		= get_sub_field('reviews_for');
				
				if($show_testimonials === true) { ?>

					<div class="container pb-90">

						<div class="row">

							<div class="col-md-12">

								<?php echo do_shortcode('[rmaa_slider reviews_for="'.$reviews_for.'" sale_type="'.$testimonial_type.'" id="'.$rma_id.'"]'); ?>

							</div>

						</div>

					</div>
								
				<?php }
							
			endif; // End testimonials



			if( get_row_layout() == 'footer_cta' ) :

				$show_footer_cta = get_sub_field('show_footer_cta');
							
				if($show_footer_cta === true) :

					get_template_part( 'global-templates/footer-cta' );
										
				endif;
							
			endif; // End footer_cta


			if( get_row_layout() == 'jumbotron_with_form' ) : ?>

				<?php $background_image = wp_get_attachment_url(get_sub_field('background_image')); 

				if ( empty($background_image) ) {
					$wrapper_class = 'subsection';
				} else {
					$wrapper_class = 'landing-page-jumbotron';
				}
				
				?>

				<div class="wrapper pt-120 <?php echo $wrapper_class; ?>" style="background-image: url(<?php echo $background_image; ?>);">
				
					<div class="container pb-120 mb-90">
				
						<div class="row align-items-center">

							<div class="col-md-6 entry-content">

								<?php $wysiwyg_editor = get_sub_field('wysiwyg_editor'); ?>

								<?php echo $wysiwyg_editor; ?>

							</div>

							<div class="col-md-1"></div>
				
							<div class="col-md-5">

								<div class="form-container">

									<?php $form_shortcode = get_sub_field('form_shortcode'); ?>

									<?php echo do_shortcode($form_shortcode); ?>

								</div>

							</div>
				
						</div>
				
					</div>

				</div>

			<?php endif; // End right_wysiwyg_with_left_image


			if( get_row_layout() == 'full_width_form' ) : ?>
			<div class="container pt-90 pb-90">
				
				<div class="row align-items-center">
					<div class="col">

						<?php $form_shortcode = get_sub_field('form_shortcode'); ?>

						<?php echo do_shortcode($form_shortcode); ?>

					</div>
				</div>
			</div>
			<?php endif; // End full width form


			if( get_row_layout() == 'feature_list' ) : ?>

				<?php $classes = get_sub_field('bootstrap_column_class');
				if( empty($classes) ) { $classes = 'col-md-4'; }?>

				<div class="container mb-90">

					<div class="row">
			
				<?php if( have_rows('feature') ) :

    				while( have_rows('feature') ) : the_row(); ?>

						<div class="<?php echo $classes; ?> feature-container mb-5">

        				<?php 
        				//$icon = get_sub_field('icon');
						$icon_image = get_sub_field('icon_image');
						$title = get_sub_field('title');
						$description = get_sub_field('description');

						//echo $icon;
						echo '<img src="'.$icon_image.'" class="icon-image oc-icon"/>';
						echo '<h4>'.$title.'</h4>';
						echo '<p class="paragraph-2">'.$description.'</p>';
						?>

						</div>

    				<?php endwhile;

				endif; ?>

					</div>

				</div>
			
			<?php endif; // End feature_list

			if( get_row_layout() == 'blog_post_cards' ) : 
				$title = get_sub_field('section_title');
				$subtitle = get_sub_field('section_subtitle');
				?>

				<div class="container pb-90 pt-90">

					<div class="row">
						<div class="col-sm-6">
							<h2 class="mb-0"><?php echo $title; ?></h2>
							<p class="subheader mb-4"><?php echo $subtitle; ?></p>
						</div>
						<div class="col-sm-6 text-center text-sm-right pb-2 pb-sm-0">
								<a href="/real-life" target="_blank" class="btn btn-outline-primary btn-sm">More from our blog</a>
						</div>
					</div>

					<div class="row">
					<?php if( have_rows('card') ) :

						while( have_rows('card') ) : the_row(); ?>
							<?php $blog_post = get_sub_field('blog_post'); 
							if( $blog_post ): 
								$postURL = get_permalink( $blog_post->ID );
								$featured_img_url = get_the_post_thumbnail_url($blog_post->ID,'full'); 
								$title = get_the_title( $blog_post->ID );
								?>

							<div class="col-md-4 mb-20 d-flex align-items-stretch">
								<div class="card shadow-sm blog-card">
								<a href="<?= $postURL; ?>" class="stretched-link" target="_blank"><img src="<?= esc_url($featured_img_url) ?>" class="card-img-top blog-card-img" alt="..."></a>
									<div class="card-body">
										<h3 class="card-title"><?php echo esc_html($title); ?></h3>
										<div class="entry-meta">

											<?php understrap_posted_on(); ?>

										</div>
									</div>
								</div>
							</div>
							<?php endif ?>
						<?php endwhile;

					endif; ?>

				</div>

				</div>

			<?php endif; // End blog_post_cards


		endwhile; // End landing_page_builder

	endif; // End flexible content ?>

</div><!-- #page-wrapper -->

<?php
get_footer();
