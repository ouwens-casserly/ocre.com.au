<?php
/**
 * Template Name: CoreLogic Digital Appraisal
 *
 * Template for displaying the CoreLogic Digital Appraisal info.
 * Do not use except for Digital Appraisal by CoreLogic
 *
 * @package UnderStrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

while ( have_posts() ) : the_post(); ?>

<div id="daintegration"></div>

<script defer src="https://dpr.leadplus.com.au/main.js"></script>
    
    <?php get_template_part( 'loop-templates/content', 'empty' ); ?>

<?php endwhile;

get_footer();
