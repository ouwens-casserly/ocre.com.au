<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package UnderStrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$container = get_theme_mod( 'understrap_container_type' );
?>

<?php get_template_part( 'sidebar-templates/sidebar', 'footerfull' ); ?>

<div class="wrapper" id="wrapper-footer">

	<div class="<?php echo esc_attr( $container ); ?>">

		<div class="row">

			<div class="col-md-12">

				<footer class="site-footer" id="colophon">

					<div class="site-info">

						<?php ocre_site_info(); ?> 

					</div><!-- .site-info -->
					
					<div class="stafflink caption site-info">
					
						Site by <a href="https://stafflink.com.au/" target="_blank">STAFFLINK</a>
						
					</div>

				</footer><!-- #colophon -->

			</div><!--col end -->

		</div><!-- row end -->

	</div><!-- container end -->

</div><!-- wrapper end -->

</div><!-- #page we need this extra closing tag here -->

<!-- Modal -->
<div class="modal fade" id="buyer-alerts" tabindex="-1" aria-labelledby="buyer-alerts-label" aria-hidden="true">
  	<div class="modal-dialog modal-dialog-centered">
    	<div class="modal-content">
      		<div class="modal-header">
				  <h3>Buyer Alerts</h3>
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          			<span aria-hidden="true">&times;</span>
        		</button>
      		</div>
      		<div class="modal-body">
				<?php echo do_shortcode('[gravityform id="9" title="false" description="true" ajax="true"]'); ?>
      		</div>
    	</div>
  	</div>
</div>

<?php wp_footer(); ?>

</body>

</html>

