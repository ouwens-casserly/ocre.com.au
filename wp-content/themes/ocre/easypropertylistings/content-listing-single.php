<?php
/**
 * Single Property Template: OCRE
 *
 * @package     EPL
 * @subpackage  Templates/ContentListingSingle
 * @copyright   Copyright (c) 2020, Merv Barrett
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       1.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) { exit; }

global $property;

if (empty(get_field('agentbox_unique_id') ) ) {
	$agentbox_id = get_agentbox_content( $property->get_property_meta('property_unique_id') );
	update_field( 'agentbox_unique_id', $agentbox_id );
}

if ( !isset( $_GET['unlisted'] ) && get_field( 'hidden_listing' ) == '1' ) {
	wp_redirect(home_url());
}

/**
 * AGENTBOX DOCUMENTS
 * ----------------------
 * 
 * Check if this is a preview link, and use it to hit the agentbox API and get updated
 * data from their documents.
 */
// if ( isset( $_GET['unlisted'] ) && $_GET['unlisted'] == 'true' ) {
	$documents = get_agentbox_content($property->get_property_meta('property_unique_id'), true);
	while ( delete_row( 'documents', 1 ) );
	foreach ($documents->response->listing->documents as $document) {
		if ( $document->webDisplay === true ) {
			add_row('documents', ['filename' => $document->title, 'file_url' => $document->url ] );
		}
	}
// }

$maps_address = str_replace( '-', '+', clean(get_the_title() ) );
$coordinates = $property->get_property_meta('property_address_coordinates');
$garage         = $property->get_property_meta('property_garage');
$carport        = $property->get_property_meta('property_carport');
$open_parking	= $property->get_property_meta('property_open_spaces');
$parking_spaces  = intval($garage) + intval($carport) + intval($open_parking); // Total parking spaces
$date_avail = $property->get_property_meta( 'property_date_available' );
$sold_date = '';

$new_link = 'https://book.inspectrealestate.com.au/Register?AgentAccountName=OCRentals&address='.urlencode($property->post->post_title).'&type=rental';
$apply_link = 'https://2apply.com.au/Form?agentID=OCRE001&uniqueID='.$property->get_property_meta('property_unique_id');

$property_status = $property->get_property_meta('property_status');
if($property_status == 'sold') {
	$sold_span = '<span class="status-sold">sold</span>';
	$sold_date = 'Sold on ' .date('jS \of F Y', strtotime($property->get_property_meta( 'property_sold_date' )));
}


if ($property->get_property_meta('property_com_exclusivity') == '1') {
	$exclusive = '<div id="exclusive-label" class="col-md-12"><span class="oc-exclusive-label">Exclusive to OC</span></div>';
} else {
	$exclusive = '';
}

$external_links = [
	$property->get_property_meta('property_external_link_label') => $property->get_property_meta('property_external_link'),
	$property->get_property_meta('property_external_link_2_label') => $property->get_property_meta('property_external_link_2'),
	$property->get_property_meta('property_external_link_3_label') => $property->get_property_meta('property_external_link_3'),
	$property->get_property_meta('property_floorplan_label') => $property->get_property_meta('property_floorplan'),
	$property->get_property_meta('property_floorplan_2_label') => $property->get_property_meta('property_floorplan_2'),
];

$author_id = get_the_author_meta('ID');
$office_id = get_field('office', 'user_'.$author_id);

$office_args = [
	'post_type'         => 'office',
	'order'             => 'ASC',
	'orderby'           => 'title',
	'posts_per_page'    => -1,
	'p'					=> $office_id
];

?>

<div id="single-property-content">

	<header class="property-header-wrapper">

		<?php get_template_part( 'global-templates/property-gallery'); ?>

		<?php echo $exclusive; ?>

		<div class="container mt-3">

			<div class="row">

				<div class="col text-center">
	
					<div class="btn-group text-center" role="group" aria-label="Basic example">

					<?php foreach ($external_links as $key => $link ) : ?>

						<?php if(!empty($link)) : ?>

							<a href="<?php echo $link; ?>" class="btn btn-outline-secondary btn-sm" target="_blank"><?php echo $key; ?></a>
						
						<?php endif; ?>

					<?php endforeach; ?>
					</div>

				</div>

			</div>

		</div>

		<div id="property-header" class="col-md-12">
			<div class="text-center font-italic"><?php echo $sold_date; ?></div>

			<h1 class="entry-title display-1 text-center">
			
			<?php if( get_field( 'display_street_address' ) ) : ?>

				<?php the_field('display_street_address'); ?>,
				<?php do_action('ocre_property_suburb'); ?>

			<?php else : ?>

				<?php do_action( 'epl_property_title' ); ?>

			<?php endif; ?>
			
			</h1>

			<div class="features"><?php echo get_property_features(['Bed', 'Bath', 'Car', 'Land']); ?></div>

		</div>

	</header>

	<div class="container">

		<div class="row">

			<div class="col-md-3">
				
				<?php if($property_status != 'sold') : ?>
					
					<h4 class="authority"><?php echo get_property_authority();?></h4>

				<?php endif; ?>
		
				<?php do_action( 'epl_property_price_content' ); ?><br>

				<?php if ($property->post->post_type == 'rental' && $property_status == 'current') : ?>
					<div class="offer-button"><a href="<?php echo $apply_link; ?>" target="_blank" class="btn btn-sm btn-primary">Apply Now</a></div>
					<?php endif; ?>

				<?php if ($property->post->post_type == 'property' && $property_status != 'sold') : ?>

					<div class="offer-button">
						<button type="button" class="btn btn-primary btn-sm mb-2" data-toggle="modal" data-target="#inspectionModal">Get in touch</button>
					</div>
				
				<?php endif; ?>
				
				<?php do_action( 'epl_property_available_dates' ); // Rentals ?>

				<?php do_action( 'epl_property_inspection_times' ); ?>

					<?php if ($property->post->post_type == 'property' && $property_status =='current') : ?>
						<button type="button" class="btn btn-outline-primary btn-sm" data-toggle="modal" data-target="#inspectionModal">Request an inspection</button>
					<?php endif; ?>

					<?php if ($property->post->post_type == 'rental' && $property_status == 'current') : ?>
						<a href="<?php echo $new_link; ?>" target="_blank" class="btn btn-sm btn-outline-primary">Register to attend open home</a>
					<?php endif; ?>

				<?php do_action( 'epl_property_land_category' ); ?>

				<?php do_action( 'epl_property_commercial_category' ); ?>

				<?php do_action( 'epl_property_tab_section' ); ?>

				
				<?php 
				if( have_rows('documents') ) : ?>

					<div class="documents epl-button-wrapper">
					<h4>Documents</h4>
				    <?php while( have_rows('documents') ) :
						the_row();
				        $filename = get_sub_field('filename');
						$file_url = get_sub_field('file_url');
					?>
						<a href="<?php echo $file_url; ?>" target="_blank" class="btn btn-outline-secondary btn-sm mb-2"><?php echo $filename; ?></a>
				
					<?php endwhile; ?>

					</div>
				<?php endif; ?>

				<div class="rates">

					<?php echo property_rates(); ?>

				</div>
			


				<div class="property-alerts">

					<h4>Buyer Alerts</h4>
					<p>Be first and sign up to receive exclusive listings from Ouwens Casserly before they hit the full market! We have new properties launch daily.</p>
					<a href="#buyer-alerts" class="btn btn-outline-primary btn-sm" data-toggle="modal">Set up Buyer Alerts</a>

				</div>

				<?php if ($property->post->post_type == 'property' && $property_status != 'sold') : ?>

					<div class="epl-button-wrapper">

					<h4>Property Reports</h4>
					<a href="<?php echo before_you_bid(); ?>" class="btn btn-outline-secondary btn-sm" target="_blank">Request a building and pest report</a>

				</div>

				<?php endif; ?>

			</div>

			<div class="col-md-8 offset-md-1">

				<h2 id="secondary-title" class="display-2"><?php do_action( 'epl_property_heading' ); ?></h2>

				<div class="property-content mb-5">

					<?php do_action( 'epl_property_the_content' );?>

				</div>

				<h4 class="mb-3">Agent</h4>

				<?php echo oc_agent_meta(get_the_author_meta('ID'), 'single-listing-card'); ?>

				<?php 
					if(!empty($property->meta['property_second_agent'][0])) {
						echo '<div class="mb-3"></div>';
						$second_agent = get_user_by('slug',  $property->meta['property_second_agent'][0] );
						echo oc_agent_meta($second_agent->data->ID, 'single-listing-card'); 
					}
				
				?>

			</div>

		</div>

		<div id="property-map" class="col-md-12 mt-90 pb-120">

			<div class="mb-3"><?php //do_action( 'epl_property_address' ); ?></div>
		
			<?php //do_action( 'epl_property_map' ); ?>
			<iframe
  height="350"
  style="border:0; width: 100%;"
  loading="lazy"
  allowfullscreen
  frameborder="0"
  src="https://www.google.com/maps/embed/v1/place?key=AIzaSyCc8hYoFOprYyBtsLccQaAv4OGHZph-9Jc
	&q=<?php echo $maps_address; ?>&zoom=17">
</iframe>

		</div>

	</div>

</div>

<?php //get_template_part( 'global-templates/similar-properties' ); ?>

<!-- Begin "looking to sell" section -->
<div class="wrapper pt-90 pb-90 mt-5">

    <div class="container">

        <div class="row mb-5">

            <div class="col-md-6 text-center offset-md-3">

				<?php if(($property->post_type == 'rental')) : ?>

					<h2 class="display-2 italic">
					Do you own a property in <span><br><?php echo $property->get_property_meta('property_address_suburb'); ?>?</span>
				</h2>

				<p class="subheading-1">
					Ouwens Casserly Real Estate is one of South Australia’s largest privately owned 
					residential and development real estate agent. If you are looking for a property manager in 
					<?php echo $property->get_property_meta('property_address_suburb'); ?>, then we can help you.
				<p>

				<?php else: ?>

                <h2 class="display-2 italic">
					Looking to sell in <span><br><?php echo $property->get_property_meta('property_address_suburb'); ?>?</span>
				</h2>

				<p class="subheading-1">
					Ouwens Casserly Real Estate is one of South Australia’s largest privately owned 
					residential and development real estate agent. If you are looking for a real estate agent in 
					<?php echo $property->get_property_meta('property_address_suburb'); ?>, then we can help you.
				<p>

				<?php endif; ?>
					
				<div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups text-center">
                    
					<div class="btn-group mr-2" role="group" aria-label="First group">

                        <a href="/digital-appraisal" type="button" class="btn btn-outline-primary btn-sm">Get an instant appraisal</a>

                    </div>

                    <div class="btn-group mr-2" role="group" aria-label="Second group">
                    
						<a href="/contact-us/" type="button" class="btn btn-link btn-sm">Drop us a line</a>
                    
					</div>

                </div>

            </div>

		</div>

    </div>

</div><!-- End "looking to sell" section -->
    

<!-- Modal -->
<div class="modal" id="inspectionModal" tabindex="-1" aria-labelledby="inspectionModallabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
    	<div class="modal-content">
      		<div class="modal-header">
        		<h5 class="modal-title" id="inspectionModallabel">Request an inspection</h5>
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      		</div>
      		<div class="modal-body">
				<div class="mb-3">
					
			  		<?php echo oc_agent_meta(get_the_author_meta('ID'), 'single-listing-card'); ?>
				</div>
				<?php echo do_shortcode('[gravityform id="6" title="false" description="false" ajax="true" field_values="property_id='.$property->get_property_meta('property_unique_id').'&property_address='.get_the_title().'&agent_name='.get_the_author_meta('display_name').'&agent_email='.get_the_author_meta('user_email').'&secondary_agent_email='.get_the_author_meta('secondary_email_address').'"]'); ?>
      		</div>
    	</div>
  	</div>
</div>

<!-- Property mobile contact menu -->
<div class="mobile-footer-buttons">

	<div class="btn-group" role="group" aria-label="Basic example">

		<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#inspectionModal">Enquire</button>

	  	<a href="tel: <?php echo str_replace('tel:', '', get_user_meta(get_the_author_meta('ID'), 'mobile', true) ); ?>"class="btn btn-outline-primary btn-sm">Call</a>

	</div>

</div><!-- End mobile contact menu -->

