<?php
/**
 * Author Box: Details Tab
 *
 * @package     EPL
 * @subpackage  Templates/ContentAuthorBoxTabDetails
 * @copyright   Copyright (c) 2020, Merv Barrett
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       3.2
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>

<!-- Author Box Container Tabbed Content -->
<div class="agent-contact-card polaroid-property">

	<h5 class="epl-author-title author-title">
		<a href="<?php echo esc_url( $permalink ); ?>">
			<?php echo esc_attr( $author_title ); ?>
		</a>
	</h5>

	<div class="epl-author-position author-position">
		<span class="label-position"></span>
		<span class="position"><?php echo esc_html( $epl_author->get_author_position() ); ?></span>
	</div>

	<div class="epl-author-contact author-contact">
		<span class="label-mobile"></span>
		<span class="mobile"><?php echo esc_html( $epl_author->get_author_mobile() ); ?></span>
	</div>

	<div class="epl-author-contact author-contact author-contact-office-phone">
		<span class="label-office-phone"></span>
		<span class="office-phone"><?php echo esc_html( $epl_author->get_author_office_phone() ); ?></span>
	</div>
</div>


