<?php
/**
 * Loop Property Template: Default
 *
 * @package     EPL
 * @subpackage  Templates/LoopListingBlogDefault
 * @copyright   Copyright (c) 2019, Merv Barrett
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       1.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $property;
$prop = get_post_meta(get_the_ID());

if ($prop['property_status'][0] == 'offmarket') {
	$exclusive = '<span class="oc-exclusive-label">OC Exclusive</span>';
} else {
	$exclusive = '';
}

if ( get_field('display_street_address' ) ) {
	$street_address = get_field('display_street_address');
} else {
	$street_address = $prop['property_address_street_number'][0].' '.$prop['property_address_street'][0];
}

?>

<div id="post-<?php the_ID(); ?>" <?php post_class( 'col-xs-1-12 col-md-4 mb-5 property-grid-item' ); ?> <?php do_action( 'epl_archive_listing_atts' ); ?>>

    <div class="polaroid-property">

		<a href="<?php echo get_permalink(); ?>">
		
		<?php if ( has_post_thumbnail() ) : ?>

			<?php epl_property_archive_featured_image('property-grid', 'property-image', false); ?>

		<?php else: ?>

			<img src="<?php echo get_template_directory_uri().'/media/placeholder.jpg'; ?>" class="property-image" />

		<?php endif; ?>

        <div class="post-content">
    
			<div class="property-header">
            
				<h5 class=""><?php echo $street_address; ?></h5>
                
				<p class="suburb"><?php echo $prop['property_address_suburb'][0]; ?></p>
                
				<p class="caption"><?php if($prop['property_status'][0] != 'sold'): ?><?php echo $prop['property_heading'][0]; ?><?php endif; ?></p>

            </div>

			<div class="property-footer">
                                
				<p class="property-price">
					<?php the_sold_date($prop); ?>
				</p>
				
                
				<div class="features"><?php echo get_property_features(); ?></div>

           	</div>
            
		</div>

	</a>
        
	</div>
    
</div>
