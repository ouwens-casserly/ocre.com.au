<?php
/**
 * The template for displaying the Make an offer page.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package UnderStrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

if ( !isset( $_GET['property_address']) ) {
    header('Location: https://'.$_SERVER['SERVER_NAME']);
    exit;
} else {
    $property_address = $_GET['property_address'];
}

get_header();

$container = get_theme_mod( 'understrap_container_type' );


if ( isset( $_GET['agent'] ) ) { $agent = $_GET['agent']; }
if ( isset( $_GET['price'] ) ) { $price = $_GET['price']; }

?>

<div class="wrapper" id="page-wrapper">

	<div class="container" id="content" tabindex="-1">

		<div class="row">

            <div class="col-md-8 offset-md-2 content-area" id="primary">

			    <main class="site-main" id="main">

			    	<?php while ( have_posts() ) : the_post(); ?>

                    <article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

                        <header class="text-center mb-5 mt-5">   
                            <span class="block">MAKE AN OFFER</span>
                            <h1 class="display-2"><?php echo $property_address; ?> </h1>
                            <div class="price"><?php echo $price; ?></div>
                        </header><!-- .entry-header -->

                        <?php if ( isset( $_GET['property_id'] ) ) : ?>

                        <div id="page-featured-image" class="mb-5 offer-image"><?php echo get_the_post_thumbnail( $_GET['property_id'], 'large' ); ?></div>

                        <?php endif; ?>

                        <div class="entry-content">

                            <?php the_content(); ?>

                            <?php echo do_shortcode('[gravityform id="1" title="false" description="false" ajax="false"]'); ?>

                        </div><!-- .entry-content -->

                    </article><!-- #post-## -->			    	

			    	<?php endwhile; ?>

			    </main><!-- #main -->

            </div> <!-- I don't know why this needs to be here but it does -->

		</div><!-- .row -->

	</div><!-- #content -->

</div><!-- #page-wrapper -->

<?php
get_footer();
