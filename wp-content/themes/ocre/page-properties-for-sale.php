<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package UnderStrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

$page_for_posts = get_option( 'page_for_posts' ); 

if ( has_post_thumbnail($page_for_posts) ) {
    $thumb_id = get_post_thumbnail_id( $page_for_posts);
    $url = wp_get_attachment_url( $thumb_id );
} else {
    $url = '';
}

$current_args = [
	'post_type'         => 'property',
	'order'             => 'DESC',
	'orderby'           => 'date',
	'posts_per_page'    => 3,
	'update_post_meta_cache' => false,
	'update_post_term_cache' => false,
	'no_found_rows'         => false,
	'meta_query' => [
		[
			'key'     => 'property_status',
			'value'   => 'current',
			'compare' => 'LIKE',
		]]
];

$sold_args = [
	'post_type'         => 'property',
	'order'             => 'DESC',
	'orderby'           => 'date',
	'posts_per_page'    => 3,
	'update_post_meta_cache' => false,
	'update_post_term_cache' => false,
	'no_found_rows'         => false,
	'meta_query' => [
		[
			'key'     => 'property_status',
			'value'   => 'sold',
			'compare' => 'LIKE',
		]]
];

$offmarket_args = [
	'post_type'         => 'property',
	'order'             => 'DESC',
	'orderby'           => 'date',
	'posts_per_page'    => 6,
	'update_post_meta_cache' => false,
	'update_post_term_cache' => false,
	'no_found_rows'         => false,
	'meta_query' => [
		'relation'=>'AND',
		[
			'key'     => 'property_com_exclusivity',
			'value'   => '1',
			'compare' => 'LIKE',
		],
		[
			'key'=>'property_status',
			'value'=>'current'
		]
	]
];


?>

<div class="wrapper" id="page-wrapper">

    <div class="property-search-hero" style="background-image: url(<?php echo $url; ?>);">

        <div id="hero" class="container">

          <div class="row">

            <div class="col-md-8 offset-md-2 text-center">

                <h1 class="display-1 text-light font-italic">Search Properties For <span>Sale</span></h1>

                <p class="subheading-1 text-light">Buy, rent, browse our recently sold properties, or find an agent to help with your property journey.</p>

            </div>

        </div>

      </div>

    </div>

	<?php get_template_part('global-templates/property-search-form'); ?>

	<div class="container">
		<div class="row">
			<div class="col-12">
				<div id="map">

				</div>
			</div>
		</div>
	</div>

	<div class="container mt-120 mb-120" id="content" tabindex="-1">

		<div class="row">

			<!-- Do the left sidebar check -->
			<?php get_template_part( 'global-templates/left-sidebar-check' ); ?>

			<main class="site-main" id="main">

				<?php while ( have_posts() ) { the_post(); } ?>

				<div class="row">
					<div class="col-md-6">
						<h2 class="mb-0">Latest Homes for Sale</h2>
						<p class="subheader mb-4 subheading">Properties available for sale by Ouwens Casserly</p>
					</div>
					<div class="col-md-6 desktop-only">
						<div class="btn-group float-right" role="group">
							<a href="#buyer-alerts" data-toggle="modal" class="btn btn-sm btn-link">Buyer Alerts</a>
							<a href="/property/?action=epl_search&post_type=property&property_status=current" class="btn btn-outline-primary btn-sm float-right sm-100">View all properties for sale</a>
						</div>
					</div>
				</div>

				<div class="row">
				
					<?php oc_property_grid($current_args); ?>

					<div class="col-md-12 mobile-only">
						<a href="/property/?action=epl_search&post_type=property&property_status=current" class="btn btn-outline-primary btn-sm float-right sm-100">View all properties for sale</a>
					</div>
				
				</div>

				<div class="row mt-90">
					<div class="col-md-6">
						<h2 class="mb-0">OC Exclusive Listings</h2>
						<p class="subheader mb-4 subheading">Only available through Ouwens Casserly Real Estate</p>
					</div>
					<div class="col-md-6 desktop-only">
						<div class="btn-group float-right" role="group">
							<a href="#buyer-alerts" data-toggle="modal" class="btn btn-sm btn-link">Buyer Alerts</a>
							<a href="/sales-oc-exclusive/" class="btn btn-outline-primary btn-sm float-right sm-100">View all exclusive properties</a>
						</div>
					</div>
				</div>

				<div class="row">
				
					<?php oc_property_grid($offmarket_args); ?>

					<div class="col-md-12 mobile-only">
						<a href="/sales-oc-exclusive/" class="btn btn-outline-primary btn-sm float-right sm-100">View all exclusive properties</a>
					</div>
				
				</div>

				<div class="row mt-90">
					<div class="col-md-6">
						<h2 class="mb-0">Recently sold properties</h2>
						<p class="subheader mb-4 subheading">Properties sold by Ouwens Casserly</p>
					</div>
					<div class="col-md-6 desktop-only">
						<div class="btn-group float-right" role="group">
							<a href="#buyer-alerts" data-toggle="modal" class="btn btn-sm btn-link">Buyer Alerts</a>
							<a href="/property/?action=epl_search&post_type=property&property_status=sold" class="btn btn-outline-primary btn-sm float-right sm-100">View all sold properties</a>
						</div>
					</div>
				</div>

				<div class="row">
				
					<?php oc_property_grid($sold_args); ?>

					<div class="col-md-12 mobile-only">
						<a href="/property/?action=epl_search&post_type=property&property_status=sold" class="btn btn-outline-primary btn-sm float-right sm-100">View all sold properties</a>
					</div>
				
				</div>
            
			</main><!-- #main -->

			<!-- Do the right sidebar check -->
			<?php get_template_part( 'global-templates/right-sidebar-check' ); ?>

		</div><!-- .row -->

	</div><!-- #content -->

	<!-- <div class="container mt-90">

		<div class="row">

			<div class="col-md-8">

				<h2 class="mb-0">Testimonials</h2>
				<p class="subheader mb-4 subheading">See what our clients are saying</p>

			</div>

			<div class="col-md-4 desktop-only">
				<a href="https://www.ratemyagent.com.au/real-estate-agency/ouwens-casserly-real-estate-ak213/sales/reviews" class="btn btn-outline-primary btn-sm float-right sm-100" target="_blank">View all on Rate My Agent</a>
			</div>

		</div>
		
		<div class="row pb-120">

			<div class="col-md-12">

				<?php //echo do_shortcode('[rmaa_slider]'); ?>

				<div class="col-md-4 mobile-only">
					<a href="https://www.ratemyagent.com.au/real-estate-agency/ouwens-casserly-real-estate-ak213/sales/reviews" class="btn btn-outline-primary btn-sm float-right sm-100" target="_blank">View all on Rate My Agent</a>
				</div>

			</div>

		</div>

	</div> -->

	<?php get_template_part( 'global-templates/footer-cta' ); ?>	

</div><!-- #page-wrapper -->


<script>
	
jQuery(document).ready(function($) {
	google.maps.event.addDomListener(window, "load", initMap);
});

function initMap() {

	var map = new google.maps.Map(document.getElementById('map'), {
		zoom: 10,
		center: { lat: -34.921230, lng: 138.599503 }, //adelaide
		mapId: 'c5ea0a8e5f3c2343'
	});

	var icon = {
		url: "/wp-content/themes/ocre/media/pin.svg",
		scaledSize: new google.maps.Size(30, 56), // scaled size
		origin: new google.maps.Point(0,0), // origin
		anchor: new google.maps.Point(0, 0) // anchor
	};

	var infoWin = new google.maps.InfoWindow();

	var markers = locations.map(function(location, i) {
  		
		var marker = new google.maps.Marker({
			position: { 
 				'lat':parseFloat(location['lat']), 
				'lng':parseFloat(location['lng'])
 			},
			icon: icon
  		});
  		google.maps.event.addListener(marker, 'click', function(evt) {
			infoWin.setContent(unescape(location['property_id']));
			infoWin.open(map, marker);
  		})
  		return marker;
	});

	var clusterStyles = [
  		{
    		textColor: 'white',
    		url: '/wp-content/themes/ocre/media/m3.png',
    		height: 50,
    		width: 50
  		},
 		{
    		textColor: 'white',
    		url: '/wp-content/themes/ocre/media/m3.png',
    		height: 50,
    		width: 50
  		},
 		{
    		textColor: 'white',
    		url: '/wp-content/themes/ocre/media/m3.png',
    		height: 50,
    		width: 50
  		}
	];

	var mcOptions = {
    	gridSize: 50,
    	styles: clusterStyles,
    	maxZoom: 15
	};
	var markerCluster = new MarkerClusterer(map, markers, mcOptions);

}

var locations = JSON.parse('<?php echo get_property_locations_json('property');?>');

</script>

<?php
get_footer();
