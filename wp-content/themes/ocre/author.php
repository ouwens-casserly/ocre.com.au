<?php
/**
 * The template for displaying the author pages
 *
 * Learn more: https://codex.wordpress.org/Author_Templates
 *
 * @package UnderStrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

generate_vcard($author);
$filepath = get_template_directory_uri()."/vcard/".$author.'.vcf';
$agent 		= get_fields('user_'. $author); // Get ACF meta fields like the agent signature
$user_meta 	= get_user_meta($author); // Meta fields only.
$author_email = get_the_author_meta('user_email', $author);

if(!empty($agent['core_market_suburb'][0]->name)) :
	$author_image_alt = $user_meta['first_name'][0] .' '.$user_meta['last_name'][0].', '.$user_meta['position'][0]. ' at Ouwens Casserly in '.$agent['core_market_suburb'][0]->name;
else:
	$author_image_alt = $user_meta['first_name'][0] .' '.$user_meta['last_name'][0].', '.$user_meta['position'][0]. ' at Ouwens Casserly at '.get_the_title($agent['office']);
endif;

if ($agent['banner_image']['url']) : 

	$bb = 'background-image: url('.$agent['banner_image']['url'].')"'; 

else : 

	$bb = 'background-image: url('.get_stylesheet_directory_uri() . '/images/background.jpg);';

endif; // Banner Image Check

?>

<article>
	<div class="wrapper single-article mb-5" id="header-wrapper" style="<?php echo $bb; ?>">
		<div id="author-name-container" class="container">
			<div class="row">
				<div class="col" style="display: flex; justify-content: center; flex-direction: column;">
					<h2 class="display-1 text-white text-center italic">Hi, I'm <?php echo $user_meta['first_name'][0] .' '.$user_meta['last_name'][0]; ?></h2>
				</div>
			</div>	
		</div>
	</div>
	<div class="wrapper" id="single-wrapper">
		<div class="container" id="content" tabindex="-1">
			<div class="row>">
				<div class="col-md-10 offset-md-1">
				<p id="breadcrumbs">
					<span>
						<span>
							<a href="/">Home</a> / <a href="/meet-the-team/">Meet the team </a> / 	
							<span class="breadcrumb_last" aria-current="page">
								<?php echo $user_meta['first_name'][0] .' '.$user_meta['last_name'][0]; ?>
							</span>
						</span>
					</span>
				</p>
			</div>
			<div class="row mb-5">
				<div id="agent-overview" class="col-md-4 offset-md-1">	
					<?php if ( $agent['static_profile_image']['url']) : ?>
					<img src="<?php echo esc_url($agent['static_profile_image']['sizes']['medium_large']); ?>" alt="<?php echo $author_image_alt;?>">
					<div id="agent-button-group" class="mt-2">
						<?php if ( $agent['show_get_an_appraisal_on_page'] === true ) : ?>
						<a class="btn btn-secondary btn-sm" href="/sales-appraisal/?agent_email=<?php echo $author_email; ?>">Get a sales appraisal</a>
						<?php endif; ?>
						<div class="btn-group">
							<?php if(!empty($user_meta['mobile'][0])) : ?>
							<a class="btn btn-primary btn-sm" href="tel: <?php echo str_replace('tel:', '', $user_meta['mobile'][0]);?>"><i data-feather="phone"></i><?php echo str_replace('tel:', '', $user_meta['mobile'][0]);?></a>
							<?php endif; ?>
							<a class="btn btn-outline-primary btn-sm" href="#ask-a-question" data-toggle="modal" data-target="#ask-a-question">Ask a Question</a>
						</div>
						<a class="btn btn-outline-secondary btn-sm" href="<?php echo $filepath; ?>">Download Contact Card</a>
						<?php if ( $agent['show_get_an_appraisal_on_page'] === true ) : ?>
						<a class="btn btn-link btn-sm" href="/property-report/">Get an Instant Appraisal</a>
						<?php endif; ?>
					</div>
					<?php endif; ?>
				</div>
				<div class="col-md-6 entry-content pl-5">
					<div class="staff-description mb-5">
						<h2 class="display-2 italic">Let's make it happen.</h2>
						<h1 class="position mb-2">
						<?php if(!empty($agent['core_market_suburb'][0]->name)) : ?>
							<?php echo $user_meta['position'][0]; ?> 
							at Ouwens Casserly in 
							<span><?php echo $agent['core_market_suburb'][0]->name; ?></span>
							<?php else: ?>
							<?php echo $user_meta['position'][0]; ?> 
							at Ouwens Casserly in <span><?php echo get_the_title($agent['office']); ?>.</span>
							<?php endif; ?>
						</h1>
						<?php echo nl2br($user_meta['description'][0]); ?>
						<?php if( get_field('youtube_video_link', 'user_'.$author) ) : ?>
						<div class="embed-container mt-3">
							<?php the_field('youtube_video_link', 'user_'.$author); ?>
						</div>
						<?php endif; ?>
					<div class="mt-3">
						<?php if ( $agent['show_get_an_appraisal_on_page'] === true ) : ?>
						<a class="btn btn-secondary btn-sm" href="/sales-appraisal/?agent_email=<?php echo $author_email; ?>">Get a sales appraisal</a>
						<?php endif; ?>
					</div>
				</div>
				<div class="col-md-1"></div>
			</div>
		</div>
	</div>
</article>

<?php if ( $agent['show_get_an_appraisal_on_page'] === true ) : get_template_part( 'global-templates/author-grids' ); endif; ?>

<?php //get_template_part( 'global-templates/footer-cta' ); ?>	

<!-- Modal -->
<div class="modal" id="ask-a-question" tabindex="-1" aria-labelledby="inspectionModallabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="inspectionModallabel">Ask a question</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
				<div class="mb-3">
					<?php echo oc_agent_meta(get_the_author_meta('ID'), 'single-listing-card'); ?>
				</div>
				<?php echo do_shortcode('[gravityform id="6" title="false" description="false" ajax="true" field_values="agent_name='.$user_meta['first_name'][0] .' '.$user_meta['last_name'][0].'&agent_email='.$author_email.'&secondary_agent_email='.$user_meta['secondary_email_address'][0].'"]'); ?>
			</div>
		</div>
	</div>
</div>
<?php 
get_footer(); 


