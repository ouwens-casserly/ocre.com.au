<?php
/**
 * The template for displaying all single posts
 *
 * @package UnderStrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

?>

<div class="wrapper" id="single-wrapper">

<?php echo get_the_post_thumbnail( $post->ID, 'full', array( 'class' => 'post-hero' ) ); ?>

    <div class="container">

        <div class="row">

            <div class="col">

            <?php if ( function_exists('yoast_breadcrumb') ) yoast_breadcrumb( '<p id="breadcrumbs">','</p>' ); ?>

            </div>

        </div>

    </div>

	<div class="container" id="content" tabindex="-1">

		<div class="row">

            <div class="col-md-7 content-area" id="primary">

			    <main class="site-main" id="main">

				<?php while ( have_posts() ) : the_post(); ?>
					
                    <?php get_template_part( 'loop-templates/content', 'single-post' ); ?>

                <?php endwhile; ?>

			    </main><!-- #main -->

            </div>

            <div class="col-md-4 widget-area offset-md-1" id="right-sidebar" role="complementary">

                <?php dynamic_sidebar( 'right-sidebar' ); ?>

            </div><!-- #right-sidebar -->

		</div><!-- .row -->

	</div><!-- #content -->

</div><!-- #single-wrapper -->

<div class="inner-wrapper pt-1 mt-5">

    <?php get_template_part( 'global-templates/latest-news' ); ?>	

    <?php get_template_part( 'global-templates/footer-cta' ); ?>	

</div>

<?php
get_footer();
