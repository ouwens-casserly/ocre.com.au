<?php
/**
 * The template for displaying all single posts
 *
 * @package UnderStrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'understrap_container_type' );
$job_description = get_field('job_description');
$send_to = get_field('send_to');
$featured_img_url = get_the_post_thumbnail_url($post->ID, 'full'); 

if($featured_img_url != false) {
    $bb = 'background-image: url('.$featured_img_url.');';
} else {
    $bb = 'background-image: url('.get_stylesheet_directory_uri() . '/images/header-careers.jpg);';
}
?>



<div class="wrapper" id="single-wrapper">

    <div id="header-wrapper" style="<?php echo $bb; ?>"></div>

	<div class="container pt-5 pb-5" id="content" tabindex="-1">

		<div class="row">

            <div class="offset-md-2 col-md-8">

                <p id="breadcrumbs">

                    <span>

                        <span>

                            <a href="/">Home</a> / <a href="/careers">Careers</a> / 

                            <span class="breadcrumb_last" aria-current="page"><?php echo get_the_title(); ?></span>

                        </span>

                    </span>

                </p>

    			<main class="site-main" id="main">

			    <?php while ( have_posts() ) : the_post(); ?>

                    <article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

	                    <header class="entry-header">

		                    <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

		                    <div class="entry-meta">

			                    <span class="caption">Job posted by</span> <?php understrap_posted_on(); ?>

		                    </div><!-- .entry-meta -->
                            
                            <div class="form-inline mt-4">
            
                                <div class="form-group">
                                
                                    <a href="#apply" class="btn btn-primary btn-sm mr-2" data-toggle="modal" data-target="#apply">Apply Now</a>
                                
                                </div>

                                <?php if (!empty($job_description)) : ?>
                
                                <div class="form-group">
                                    
                                    <a class="btn btn-outline-primary btn-sm" href="<?php echo $job_description['url']; ?>" target="_blank" rel="noopener">Download Job Description</a></div>
        
                                <?php endif;?>

                                <?php if(!empty($send_to)) { echo '<span class="gform_description mt-3">'.$send_to.'</span>'; } ?>
            
                            </div>

	                    </header><!-- .entry-header -->

                        <hr/>

	                    <div class="entry-content">

		                    <?php the_content(); ?>

	                    </div><!-- .entry-content -->

                    </article><!-- #post-## -->
                        
                <?php endwhile; ?>
                
                <div class="entry-content">
                
                    <p> 
                                    
                        <span class="mt-3">At Ouwens Casserly Real Estate our team live and work by the following core values:</span>
                                    
                        <ul style="font-weight: 500">
                            <li>Authenticity</li>
                            <li>Optimism</li>
                            <li>Passion</li>
                            <li>Precision</li>
                        </ul>

                    </p>
                
                </div>
            
                <div class="form-group">
                    <a href="#apply" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#apply">Apply Now</a>
                </div>

		        </main><!-- #main -->

            </div>

		</div><!-- .row -->

	</div><!-- #content -->

</div><!-- #single-wrapper -->

<!-- Modal -->
<div class="modal fade" id="apply" tabindex="-1" role="dialog" aria-labelledby="applyModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Apply</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <?php echo do_shortcode('[gravityform id="10" title="false" description="false"]'); ?>
      </div>
    </div>
  </div>
</div>

<?php
get_footer();
