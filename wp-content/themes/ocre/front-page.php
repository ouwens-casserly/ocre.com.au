<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package UnderStrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

?>

<div class="wrapper" id="page-wrapper">
	<div class="hero-wrapper" style="min-height: 800px; background-image: url('/wp-content/uploads/2021/07/landing-background.png'); background-position: center center; background-size: cover; background-repeat: norepeat;">
	<?php get_template_part( 'global-templates/home-hero' ); ?>

	<div class="container" id="content" tabindex="-1">
</div>
		<div class="row">

			<div class="col-md content-area" id="primary">

				<main class="site-main" id="main">

					<?php while ( have_posts() ) { the_post(); get_template_part( 'loop-templates/content', 'home' ); } ?>

				</main><!-- #main -->

			</div>

		</div><!-- .row -->

	</div><!-- #content.container -->

	<?php get_template_part( 'global-templates/property-grids' ); ?>

	<?php get_template_part( 'global-templates/about' ); ?>

	<?php get_template_part( 'global-templates/services-masonry' ); ?>

	<div id="rma-header" class="container mt-90">

		<div class="row">

			<div class="col-md-8">

				<h2>Testimonials</h2>

				<p class="subheading subheader">See what our clients have to say</p>

			</div>

			<div class="col-md-4">

				<a href="https://www.ratemyagent.com.au/real-estate-agency/ouwens-casserly-real-estate-ak213/sales/reviews" class="btn btn-outline-primary btn-sm float-right mt-2" target="_blank">See us on Rate My Agent</a>

			</div>

		</div>

	</div>

	<div class="container">
		
		<div class="row">

			<div class="col-md-12">

				<?php echo do_shortcode('[rmaa_slider]'); ?>

			</div>

		</div>

	</div>

	<?php get_template_part( 'global-templates/latest-news' ); ?>	

	<?php get_template_part( 'global-templates/footer-cta' ); ?>	

</div><!-- #page-wrapper -->

<?php
get_footer();
