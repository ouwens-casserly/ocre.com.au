<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package UnderStrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

$container = get_theme_mod( 'understrap_container_type' );

?>

<div class="wrapper" id="page-wrapper">

    <div class="container mt-90 mb-5">
        <div class="row">
            <div class="col-md-6 offset-md-3 text-center">
                <h1 class="display-1 italic">Get in touch with <span>us</span></h1>
                <p>Our success is born of culture, professionalism and genuine desire to help people reach their goals through property.</p>
                <a href="tel: +618 7070 6488" class="btn btn-primary btn-sm">Call Us</a>
                <a href="/digital-appraisal" class="btn btn-outline-primary btn-sm">Request an appraisal</a>
            </div>
        </div>
    </div>

    <?php 

    $office_args = [
    	'post_type'         => 'office',
    	'order'             => 'ASC',
    	'orderby'           => 'title',
    	'posts_per_page'    => -1
    ];
    
    ?>

    <div class="container mt-5 mb-5">
        <div class="row">
            <?php echo oc_office_grid($office_args); ?>
        </div>
    </div>

</div><!-- #page-wrapper -->

<?php get_template_part( 'global-templates/footer-cta' ); ?>	

<?php
get_footer();
