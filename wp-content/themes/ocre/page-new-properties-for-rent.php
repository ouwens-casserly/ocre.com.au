<?php
/**
 * The template for displaying Recently Leased. Designed to be embedded in iframes.
 *
 * @package UnderStrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

$leased_args = [
	'post_type'         => 'rental',
	'order'             => 'DESC',
	'orderby'           => 'date',
	'posts_per_page'    => 60,
	'meta_query' => [
		[
			'key'     => 'property_status',
			'value'   => 'current',
			'compare' => 'LIKE',
		]]
];

?>

<div class="wrapper mt-5" id="page-wrapper">

	<div class="container" id="content" tabindex="-1">

		<div class="row">

			<?php get_template_part( 'global-templates/left-sidebar-check' ); ?>

			<main class="site-main" id="main">

				<?php while ( have_posts() ) { the_post(); } ?>

				<div class="row">

					<div class="col-md-8">

						<h1 class="mb-0">Properties for lease</h1>
						<p class="subheader mb-4">Properties for lease by Ouwens Casserly</p>

					</div>

				</div>

				<div class="row">
				
					<?php oc_property_grid($leased_args); ?>
				
				</div>
            
			</main><!-- #main -->

			<?php get_template_part( 'global-templates/right-sidebar-check' ); ?>

		</div><!-- .row -->

	</div><!-- #content -->

</div><!-- #page-wrapper -->

<style>
    #wrapper-navbar {
        display: none!important;
    }
    body {
        background-color: #f8faf5;
    }
    </style>
<?php
// get_footer();
