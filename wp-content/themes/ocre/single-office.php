<?php
/**
 * The template for displaying all single posts
 *
 * @package UnderStrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

$contact_details = get_field('contact_details');

if(empty($contact_details['phone'])) {
    $phone = $contact_details['sales'];
} else {
    $phone = $contact_details['phone'];
}

?>

<div class="wrapper" id="single-wrapper">

<?php echo get_the_post_thumbnail( $post->ID, 'full', array( 'class' => 'post-hero' ) ); ?>

    <div class="container">

        <div class="row">

            <div class="col">

            <?php if ( function_exists('yoast_breadcrumb') ) yoast_breadcrumb( '<p id="breadcrumbs">','</p>' ); ?>

            </div>

        </div>

    </div>

	<div class="container" id="content" tabindex="-1">

		<div class="row">

            <div class="col-md-12 content-area" id="primary">

			    <main class="site-main" id="main">

				<?php while ( have_posts() ) : the_post(); ?>

                    <div <?php post_class(); ?> id="post-<?php the_ID(); ?>">

                        <div class="container mb-5">

                            <div class="row">

                                <div class="col-md-6 offset-md-3">

                    	            <header class="entry-header text-center mb-3">

                    	            	<?php the_title( '<h2 class="entry-title display-1 italic">OC <span>', '</span></h2>' ); ?>

                                        <div class="excerpt mb-3">
                                            <?php the_title( '<h1 class="seo-heading">Real Estate Agents and property managers in ', '</h1>' ); ?>
                                            <?php if($post->post_excerpt != '') { echo $post->post_excerpt; } ?>
                                        
                                        </div>

                                        <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups text-center">

                                            <div class="btn-group mr-2" role="group" aria-label="First group">

                                                <a href="#single-office-team" type="button" class="btn btn-outline-primary btn-sm">Meet our team</a>

                                            </div>

                                            <div class="btn-group mr-2" role="group" aria-label="Second group">

                                                <a href="tel: <?php echo $phone; ?>" type="button" class="btn btn-link btn-sm">Drop us a line</a>

                                            </div>

                                        </div>

                    	            </header><!-- .entry-header -->

                                </div>

                            </div>

                        </div>
                            
                    	<div class="entry-content container">

                            <div class="row">

                                <div class="col-md-4 offset-md-1">

                                    <h3>Contact Us</h3>

                                    <?php echo oc_get_contact_details(); ?>

                                    <div class="contact-info">
                                        
                                        <div class="address">

                                            <div class="spanner">
                                            
                                                <span class="caption"> Address</span> 
                                                
                                                <div class="full-address"><?php echo oc_get_address(); ?></div>

                                            </div>
                                        
                                        </div>
                                    
                                    </div>

                                    <?php $location = get_field('address');
        
                                    if( $location ): ?>

                                    <!-- we need to encode the address before it goes into the url -->
                                    <?php $address = urlencode($location['address']); ?>
                                        <div class="acf-map" data-zoom="16">

                                            <iframe width="100%" height="100%" src="https://maps.google.com/maps?output=embed&hl=en&q=<?= $address ?>&query_place_id=<?= $location['place_id'] ?>&ie=UTF8&t=&z=16&iwloc=B"
                                                frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                                            </div>
                                
                                    <?php endif; ?>

                                </div>

                                <div class="col-md-5 offset-md-1">
                                    
                                    <?php the_content(); ?>

                                </div>

                            </div>
                            
                    	</div><!-- .entry-content -->
                            
                    </div><!-- #post-## -->

                <?php endwhile; ?>

			    </main><!-- #main -->

            </div>

		</div><!-- .row -->

	</div><!-- #content -->

    <div id="single-office-team" class="inner-wrapper pt-90 pb-120 mt-5">
        <div class="container">
            <div class="row mb-5">
                <div class="col-md-12 text-center">
                    <p class="subheading-1">Ouwens Casserly <?php the_title(); ?></p>
                    <h2>Our <span>Team</span></h2>
                </div>
            </div>
            <div class="row">
            <?php oc_team_list('sales', 'col-md-3 col-lg-3 single-team-member col-6', $post->ID); ?>
            </div>
        </div>
    </div>

</div><!-- #single-wrapper -->

<style type="text/css">
.acf-map {
    width: 100%;
    height: 400px;
    margin: 20px 0;
}

.acf-map img {
   max-width: inherit !important;
}

</style>


<?php
get_footer();
